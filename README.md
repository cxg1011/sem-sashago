## SEM-SASHAGO: SASHAGO with semantics
Frontend for extracting matchables from RGB-D data along with semantic
bounding boxes and building a `g2o` graph.


### Usage
#### Installation
This package is created as an extension to the `SASHAGO` package in the `SRRG` software ecosystem, available [here](https://gitlab.com/srrg-software/srrg_sashago).
Follow the instructions there to install all the dependencies, with the following alterations:

1. when installing the supporting `SRRG` packages, you need to install specific versions to ensure compatibility
    * `srrg_cmake_modules` - `656f7b9b8e182f4df1c52da83b0e3307e22e3658`
    * `srrg_core` - `3e0519006ec1fdde306d29eacd43050111b3a058`
    * `srrg_core_ros` - `129057c302c4c42dca31638a4f6ba93f4c85be62`
    * `srrg_gl_helpers` - `aef5962d7d2379a4f0fbed900e8803fbd5192636`
    * `srrg_hbst` - `00faba728cdb78a298775107e71aff956db21902`
2. in the `srrg_core` package, replace the source file `srrg_core/src/srrg_kdtree/kd_tree.hpp` with `instructions/kd_tree.hpp`
3. do not clone the `srrg_sashago` package, replace it with the current package instead

After doing the above, build the `catkin` workspace.

### Parameters
In the configuration directory, a number of `YAML` parameter files are present. Please note that these
files contain path variables, which you must adapt to your system before running. For reproducing the
publication results, please use:
1. `icl_configuration.yaml`
2. `intnet_configuration.yaml`
3. `fr1_configuration.yaml`
4. `fr3_configuration.yaml`

for the corresponding sequences.

#### Semantics
If semantics are present, they need to be exported in a text file with line format:
`timestamp [bb_tl_x, bb_tl_y, bb_br_x, bb_br_y] class_id conf_score`
If real-time extraction is required, you'll have to edit the `src/detector/SemanticDetector` module to accommodate message passing.
In the case of `InteriorNet`, the ground truth semantics are available in `JSON` format, which you can automatically
export to our format using `read_coco.py` from the `utils` directory.
For other datasets, such as `TUM RGB-D`, we ran a FasterRCNN implementation `detectron2` and exported the detections in the above format.
The original project is available [here](https://github.com/facebookresearch/detectron2), and you can use the material in the instructions directory
to deal with running the correct model and exporting in our format.

### Running
Running the system requires converting a dataset to the `TXTIO` format. There are scripts in the utils directory
to help you do that, along with instruction files with the steps necessary to convert `ICL-NUIM`, `InteriorNet` and
`TUM-RGBD` sequences to the proper format. In addition, the instructions directory contains execution scripts to help
run the system as well as to automate the evaluation procedure. Note that the evaluation scripts require the TUM evaluation
tools to be saved on your system, so adjust the paths accordingly and make sure `Python 2` is available.

### Other SLAM algorithms
The configurations directory also lists `ORBSLAM 2` parametrization files we used in our comparative
experiments. You can also find a set of instructions for setting up an NVIDIA-compatible Docker image for running `MaskFusion`
in the instructions directory.

### Visualizations
In addition to the default visualizations available in the base package, the `map_viewer` has been edited to optionally support externally supplied
colors for different semantic classes. A sample random color file is already available in the configurations directory. Set the path in the default
configuration at `map_viewer.h` to suit your environment. You can also generate different random color sequences using `color_gen.py` from utils. 




