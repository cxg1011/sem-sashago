#!/usr/bin/python3

import sys
import os

def parseDirectory(imgDir, topicStr, tarDir=None, prevDelim='/', newDelim='.', numFmtStr="_{:08}"):
    if (imgDir[-1] == '/'):
        imgDir = imgDir[:-1]
    if (topicStr[0] == '/'):
        topicStr = topicStr[1:]

    if (not tarDir):
        tarDir = imgDir
    elif (tarDir[-1] == '/'):
        tarDir = tarDir[:-1]

    img_dir = os.fsencode(imgDir)
    topicStr = topicStr.replace(prevDelim, newDelim)

    for cnt, fimg in enumerate(sorted(os.listdir(img_dir))):
        oldName = imgDir+'/'+os.fsdecode(fimg)
        newName = tarDir+'/'+topicStr+numFmtStr.format(cnt+4)+os.path.splitext(oldName)[1]
        os.rename(oldName, newName)
    
if __name__=='__main__':
    image_directory = sys.argv[1]
    topic_string = sys.argv[2]
    target_directory = None
    if (len(sys.argv) == 4):
        target_directory = sys.argv[3]
    parseDirectory(image_directory, topic_string, tarDir=target_directory)
