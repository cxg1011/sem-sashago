#!/usr/share/python

# Format specific -> goes from InteriorNet format to TUM RGB-D format
# InteriorNet <timestamp,x,y,z,qw,qx,qy,qz>
# TUM <timestamp x y z qx qy qz qw>

from copy import copy

def rearrangeList(elements):
	res = copy(elements)
	res[4:7], res[7] = res[5:], res[4]
	return res

def parseLine(rawLineData, origDelim=',', tarDelim=' '):
	line = rawLineData.strip()
	if line[0] == "#":
		return None
	
	elems = line.split(origDelim)
	outElems = rearrangeList(elems)
	return tarDelim.join(outElems)

def parseFile(inputFilePath, outputFilePath='tum_gt.txt'):
	with open(inputFilePath, 'r') as fin, open(outputFilePath, 'w') as fout:
		for l in fin:
			outLine = parseLine(l)
			if outLine:
				fout.write(outLine+'\n')

if __name__=='__main__':
	inputFilePath = 'cam0_gt.visim'
	parseFile(inputFilePath)
