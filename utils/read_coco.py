import sys
import json

def readFile(filePath, specifiers=None):
    with open(filePath) as fin:
        data = json.load(fin)
        #print(data.keys())
    return data if not specifiers else [data[s] for s in specifiers]

def jprint(jsonData):
    print(json.dumps(jsonData, indent=4, sort_keys=True))

def getTimestampFromFileName(path, delim='/', formatCode=0):
    segments = path.split(delim)
    if (formatCode == 0): 
        return segments[-1].split('.')[0]   # assuming that path is: .../<timestamp>.<filetype>
    else:
        logger.error("File name format code {} not yet implemented!".format(formatCode))
        #raise ValueError("Unacceptable file name format code.")
        return None


if __name__=='__main__':
    filePath = sys.argv[1]
    cocoData = readFile(filePath, specifiers=['annotations', 'images'])

    cocoData[0] = sorted(cocoData[0], key=lambda elem : elem['mask_image_id'])

    bboxes = [entry['bbox'] for entry in cocoData[0]]
    #print(bboxes)

    categories = [entry['category_id'] for entry in cocoData[0]]
    #print(len(categories))

    #jprint(cocoData[0][:20])

    #jprint(cocoData[1][:20])

    with open("gtAnno.txt", 'w') as fout:
        for j, anno in enumerate(cocoData[0]):
            fout.write(
                    getTimestampFromFileName(cocoData[1][int(anno['mask_image_id'])]['coco_url']) + '\t'
                    + str(anno['bbox']) + '\t'
                    + str(anno['category_id']) + '\t'
                    + '1.0\n')

    #jprint(cocoData)
