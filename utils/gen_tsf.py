#!/usr/bin/python3

import sys
import os

def parseAssoc(assocPath):
    with open(assocPath, 'r') as fin:
        return {float(line.split()[0]) : line.split()[2] for line in fin}

def parseDirectory(rgbDir, outFileName, assoc=None, idxFmtStr="{:08}"):
    ts_pairs = None
    if (assoc):
        ts_pairs = parseAssoc(assoc)

    with open(outFileName, 'w') as fout:
        if (rgbDir[-1] == '/'):
            rgbDir = rgbDir[:-1]
        
        img_dir = os.fsencode(rgbDir)
        cnt = 4
        for fimg in sorted(os.listdir(img_dir)):
            timestamp = os.path.splitext(os.fsdecode(fimg).split('/')[-1])[0]
            if (ts_pairs and float(timestamp) in ts_pairs.keys()):
                fout.write('R\t' + idxFmtStr.format(cnt) + '\t' + timestamp + '\n')
                fout.write('D\t' + idxFmtStr.format(cnt) + '\t' + ts_pairs[float(timestamp)] + '\n')
                cnt += 1
            elif (not ts_pairs):
                fout.write(idxFmtStr.format(cnt) + '\t' + timestamp + '\n')
                cnt += 1


    
if __name__=='__main__':
    rgb_directory = sys.argv[1]
    outFileName = sys.argv[2]

    assocFileName = None
    if (len(sys.argv) == 4):
        assocFileName = sys.argv[3]

    parseDirectory(rgb_directory, outFileName, assoc=assocFileName)
