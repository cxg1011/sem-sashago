#!/usr/bin/python3

import argparse
import sys
import os
import math
from PIL import Image

# Default calib params for InteriorNet
focalLength = 600.0
centerX = 320.0
centerY = 240.0

# Depth scaling factor
scalingFactor = 1.0 # I won't mess with this right now, maybe for compatibility reasons later though?

def image_reform(depth):
    """
    Get a depth image, adjust the Z value, return the resulting depth image.
    """
    if depth.mode != "I":
        raise Exception("Depth image is not in intensity format")

    depths = []
    for v in range(depth.size[1]):
        for u in range(depth.size[0]):
            D = depth.getpixel((u,v)) / scalingFactor
            #if D==0: continue
            x_img = u - centerX
            y_img = v - centerY
            Z = focalLength * D / math.sqrt(x_img**2 + y_img**2 + focalLength**2)
            #X = x_img * Z / focalLength
            #Y = y_img * Z / focalLength
            depths.append((Z))
    
    resImg = Image.new(depth.mode, depth.size)
    resImg.putdata(depths)
    return resImg

def parse_directory(inDir, outDir):
    if (inDir[-1] == '/'):
        inDir = inDir[:-1]
    if (outDir[-1] == '/'):
        outDir = outDir[:-1]

    imList = os.fsencode(inDir)
    cnt = 0
    fl = os.listdir(imList)
    for f in fl:
        imgName = os.fsdecode(f)
        print(inDir+'/'+imgName)
        oldImage = Image.open(inDir+'/'+imgName)
        reformedImage = image_reform(oldImage)
        reformedImage.save(outDir+'/'+imgName)
        print(outDir+'/'+imgName)
        cnt += 1
        print("Done with {:} out of {:}".format(cnt, len(fl)))
    
    print("Converted {:} image{} in total.".format(cnt, ('s' if cnt != 1 else '') ))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script reforms depth images from the InteriorNet format to an TUM/ICL compliant format.
    ''')
    parser.add_argument('depth_folder', help='input depth folder (image format: png)')
    parser.add_argument('output_folder', help='destination depth folder')
    args = parser.parse_args()

    parse_directory(args.depth_folder, args.output_folder)
    
