#!/usr/bin/python

import argparse

import numpy as np

def genRand(size_x, size_y, normDim=None):
    data = np.random.rand(size_x, size_y)
    if (not normDim):
        return data

    sums = np.sum(data, axis=normDim)
    if normDim == 1:
        data = (data.transpose() / sums).transpose()
    else:
        data /= sums
    return data

def dumpToFile(array, outFile):
    with open(outFile, 'w') as fout:
        sz = array.shape
        for i in range(sz[0]):
            for j in range(sz[1]):
                fout.write("{:.4f}{}".format(array[i, j], '\n' if (j == sz[1]-1) else ' '))


def main(args):
    print("Generating: {:}x{:} random entries, {}normalized{}."
            .format(args.szx, args.szy,
                "" if args.dim else "not ",
                " along axis {:}".format(args.dim) if args.dim else ""))
    res = genRand(args.szx, args.szy, normDim=args.dim)
    print(res)
    if (args.fout):
        print("Writing to file: {}".format(args.fout))
        dumpToFile(res, args.fout)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='''
    Script that generates random numbers in (0,1) in a 2D array,
    with optional sum normalization along some dimension.
    ''')
    parser.add_argument('szx', type=int, help='number of rows of the matrix')
    parser.add_argument('szy', type=int, help='number of rows of the matrix')
    parser.add_argument('--dim', '-d', type=int, choices=range(0, 2), help='optionally normalize the sum along an axis to be 1.0 (0 to normalize columns, 1 to normalize rows)')
    parser.add_argument('--fout', '-o', help='optional output file to store the result')

    args = parser.parse_args()
    main(args)

