#!/usr/share/python

from pathlib import Path
import sys

def genAssoc(rgbDir, rgbLink, depthDir, depthLink, outputFile='perfect_assoc.txt'):
	with open(outputFile, 'w') as of:
		rgbData = Path(rgbDir).glob('*')
		rgbList = [p.name for p in rgbData]
		rgbList.sort()
		
		depthData = Path(depthDir).glob('*')
		depthList = [p.name for p in depthData]
		depthList.sort()

		assert(len(rgbList) == len(depthList))

		for r, d in zip(rgbList, depthList):
			of.write(r[:-4] + ' ' + rgbLink+r + ' ' + d[:-4] + ' ' + depthLink+d + '\n')

		print("DONE, parsed {} frames.".format(len(rgbList)))

if __name__=='__main__':
	rgbDir = sys.argv[1]
	depthDir = sys.argv[2]
	rgbLink = 'rgb/'
	depthLink = 'depth/'
	genAssoc(rgbDir, rgbLink, depthDir, depthLink)
