#!/bin/bash

pre=""
if (( $# == 3 )); then
	pre="preopt_"
fi

statfile="$pre""stats.txt"

echo "ATE:" > $statfile
python /path/to/evaluate_ate.py $1 $2 --plot "$pre""ate.png" --verbose >> $statfile
echo "RPE:" >> $statfile
python /path/to/evaluate_rpe.py $1 $2 --fixed_delta --delta_unit f --plot "$pre"rpe.png --verbose >> $statfile
cat $statfile
