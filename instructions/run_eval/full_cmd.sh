#!/bin/bash

cmd_arg=""
precmd=""
if (( $# == 0 )); then
	echo "--> Default run"
	pre_cmd="xvfb-run --auto-servernum --server-num=1 -s \"-screen 0 640x480x24\""
elif [[ "$1" == "det_tune" ]]; then
	echo "--> Detector tuning"
	cmd_arg="1"
elif [[ "$1" == "rel_tune" ]]; then
	echo "--> Relocalizer tuning"
	cmd_arg="2"
else
	echo "Unacceptable argument: $1"
	exit 1
fi

eval "$pre_cmd ./command.sh $cmd_arg"
