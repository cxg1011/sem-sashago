#!/bin/bash

registry="/path/to/TXTIO/file/name.txtio"

config_path="/path/to/some_configuration.yaml"

if (( $#==0 )); then
	rosrun srrg_sashago sashago_txtio_app $config_path  $registry
elif (( $1==1 )); then
	rosrun srrg_sashago sashago_detection_tuner_app $config_path $registry
elif (( $1==2 )); then
	rosrun srrg_sashago sashago_relocalizer_tuner_app $config_path $registry
else
	echo "Unexpected argument $1"
	exit 1
fi
