## Running the system

To do a run of the system:
1. source the devel/setup.bash file from your catkin workspace
2. you can then directly use the `command.sh` script to take care of running (remember to edit the paths)
3. in case you are in a headless environment, consider `full_cmd.sh` to avoid opening a GUI

## Evaluating

The system's output after a complete run should be 2 `.sashago` files. They will be used for the evaluation.
Start by creating an empty directory, then put `eval.sh` in it, along with the `tum_gt.txt` file, aka the
groundtruth trajectory in TUM RGB-D compliant form. Then, run `post_cmd.sh` with the above directory as its
only argument. Make sure the TUM evaluation tool paths are correct before running. The outputs should be
stats files containing error metrics (before and after Global Optimization), along with `PNG` visualizations
for the trajectories. We recommend commenting out the difference plotting (in red color) from the ATE evaluation tool
to improve legibility of the output.
