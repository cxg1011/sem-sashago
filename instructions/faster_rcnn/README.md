### How to run FasterRCNN from detectron 2's suite

Input in our use case is a directory of RGB images (only tested with `.png` format)
Output should be a directory full of annotated RGB images and a `predOutput.txt` file containing the predicted bounding boxes in our format

1. install [detectron2](https://github.com/facebookresearch/detectron2)
2. replace `demo/demo.py` with the file provided here
3. copy the fragments of the `command.sh` into a terminal, replacing paths as you go
4. input path should be like this: `... --input /path/to/dir/* ...`
5. After running, inspect the output. In case you need to merge classes together in super-classes, or remove detections, use:
    ```bash
    $ python class_batcher.py orig_detections.txt equivs.txt final_detections.txt [-d dels.txt]
    ```

For step 5, pay attention to the class index offset explained in the first lines of `class_batcher.py`!
