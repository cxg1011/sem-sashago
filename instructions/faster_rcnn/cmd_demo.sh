#!/bin/bash

python demo.py --config-file ../configs/COCO-Detection/faster_rcnn_R_101_FPN_3x.yaml \
  --input $1 \
  --output $2 \
  --opts MODEL.WEIGHTS detectron2://COCO-Detection/faster_rcnn_R_101_FPN_3x/137851257/model_final_f6e8b1.pkl
