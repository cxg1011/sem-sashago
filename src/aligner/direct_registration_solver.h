#pragma once
#include "base_registration_solver.h"

namespace srrg_sashago {

  class DirectRegistrationSolver : public BaseRegistrationSolver {
    public:
      // constructs an empty solver
      DirectRegistrationSolver();

      //
      ~DirectRegistrationSolver();

      // prepares the optimization, starting from the specified pose
      void init(const Isometry3& transform_ = Isometry3::Identity(),
                const bool use_support_weight = false) override;

      // executes one round of optimization
      void oneRound() override;

    protected:
      // computes the error and the Jacobian of a constraint
      void errorAndJacobian(Vector3& e_p,    // point-error
                            Vector3& e_d,    // direction-error
                            real& e_o,              // orthogonality error
                            real& e_x,              // intersection error
                            Matrix3_12& J_p,  // point jacobian
                            Matrix3_12& J_d,  // direction jacobian
                            Matrix1_12& J_o,  // orthogonality jacobian
                            Matrix1_12& J_x,  // orthogonality error
                            bool compute_p,          // if true computes the point part
                            bool compute_d,          // if true computes the direction part
                            bool compute_o,          // if true computes the orthogonality part
                            bool compute_x,          // if true computes the orthogonality part
                            const Matchable& fixed_, // input fixed item
                            const Matchable& moving); // input moving item

      // generic constraint linearization
      // side effect on _H, _b and the constrant vector to write the error
      // of each constraint
      void linearizeConstraint(Constraint& constraint);    // set to true if the constraint is an intersection

      static inline Vector12 flattenIsometry(const Isometry3& iso);
      static inline Isometry3 unflattenIsometry(const Vector12& v);
      static inline Matrix6_12 jacobianT(const Vector3& point, const Vector3& direction);

      void oneRoundUpper();
      void oneRoundLower();
      Matrix12 _H;
      Vector12 _b;

    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  };
  
} //ia end namespace srrg_bagasha
