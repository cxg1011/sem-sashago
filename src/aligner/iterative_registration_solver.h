#pragma once
#include "base_registration_solver.h"

namespace srrg_sashago {

  class IterativeRegistrationSolver : public BaseRegistrationSolver {
    public:
      //! @brief ctor-dtor
      IterativeRegistrationSolver();
      virtual ~IterativeRegistrationSolver();

      // prepares the optimization, starting from the specified pose
      void init(const Isometry3& transform_ = Isometry3::Identity(),
                const bool use_support_weight = false) override;

      // executes one round of optimization
      void oneRound() override;

      inline const real chiSquare() {return _chi_square/(real)_num_inliers;}
      inline const size_t numInliers() {return _num_inliers;}

    protected:

      // computes the error and the Jacobian of a constraint
      void errorAndJacobian(Vector3& e_p,    // point-error
                            Vector3& e_d,    // direction-error
                            real& e_o,              // orthogonality error
                            real& e_x,              // intersection error
                            Matrix3_6& J_p,  // point jacobian
                            Matrix3_6& J_d,  // direction jacobian
                            Matrix1_6& J_o,  // orthogonality jacobian
                            Matrix1_6& J_x,  // orthogonality error
                            bool compute_p,          // if true computes the point part
                            bool compute_d,          // if true computes the direction part
                            bool compute_o,          // if true computes the orthogonality part
                            bool compute_x,          // if true computes the orthogonality part
                            const Matchable& fixed,  // input fixed item
                            const Matchable& moving, // input moving item
                            const bool rotate_omega = false);

      // generic constraint linearization
      // side effect on _H, _b and the constrant vector to write the error
      // of each constraint
      void linearizeConstraint(Constraint& constraint);    // set to true if the constraint is an intersection

      Matrix6 _H;
      Vector6 _b;

      real _chi_square;
      size_t _num_inliers;

    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  };
  
} //ia end namespace srrg_bagasha
