#pragma once
#include <vector>
#include <algorithm>
#include <g2o/core/optimizable_graph.h>
#include "scene_entry.h"
#include "scene.h"

namespace srrg_sashago {

  class Landmark {
  public:
    Landmark() = delete;

    //ia get methods
    inline const size_t& id() const {return _id;}
    inline SceneEntry* entry() const {return _entry_ptr;}
    inline SceneEntry* origin() const {return _origin;}
    inline g2o::HyperGraph::Vertex* vertexPtr() const {return _vertex_ptr;}
    inline const g2o::HyperGraph::EdgeSet& edges() const {return _edges;}

    //ia set methods
    //ia when a new edge is created we want to do some bookkeeping
    inline void setVertexPtr(g2o::HyperGraph::Vertex* new_matchable_vertex_) {_vertex_ptr = new_matchable_vertex_;}
    inline void addEdge(g2o::HyperGraph::Edge* new_matchable_edge_) {_edges.insert(new_matchable_edge_);}

    //! @brief merges a landmark with another landmark - does not free the memory
    //! @param[in] landmark_: the landmark to be merged; it should be deleted after merging
    void merge(Landmark* landmark_);

	// Semantic "best" class for landmark
	int getDominantClassID();

	// Average XYZ position, for denoising purposes
	Vector3 getAvgPosition();
	Vector3 getMedPosition();


  protected:
    //ia protected ctor/dtor - only the map can create a new landmark
    Landmark(SceneEntry* entry_, const size_t& id_);
    virtual ~Landmark();

    SceneEntry* _entry_ptr = 0;                         //! @brief the entry related to this landmark - I already have all the prev history
    SceneEntry* _origin    = 0;                         //! @brief the first entry relative to this landmark
    g2o::HyperGraph::Vertex* _vertex_ptr = 0;           //! @brief g2o ptr to the vertex created from this landmark
    g2o::HyperGraph::EdgeSet _edges;                    //! @brief set of g2o edges related this pointer

    const size_t _id;

  public:
    friend class WorldMap;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

  using LandmarkVector = std::vector<Landmark*, Eigen::aligned_allocator<Landmark*> >;
  using LandmarkSet = std::set<Landmark*, std::less<Landmark*>, Eigen::aligned_allocator<Landmark*> >;
  using IntLandmarkMap = std::unordered_map<int, Landmark*, std::hash<int>, std::equal_to<int>, Eigen::aligned_allocator<std::pair<int, Landmark*> > >;


} //ia end namespace srrg_bagasha

