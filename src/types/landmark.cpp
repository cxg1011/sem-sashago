#include "landmark.h"
#include <iostream>
#include <map>

namespace srrg_sashago {

  Landmark::Landmark(SceneEntry* entry_,
                     const size_t& id_) :
                         _id(id_) {
    _entry_ptr = entry_;
    _entry_ptr->_landmark_ptr = this;

    assert(!_entry_ptr->nextSceneEntry() && "[Landamark::Landmark] unexpected error");

    SceneEntry* e = _entry_ptr;
    while (e->_prev) {
      e->_prev->_landmark_ptr = this;
      e = e->_prev;
    }
    _origin = e;

    assert(_origin && "[Landamark::Landmark] invalid origin");
    assert(_origin->landmark() == this && "[Landamark::Landmark] unexpected error");
  }

  Landmark::~Landmark() {}

  void Landmark::merge(Landmark* landmark_) {
    assert(landmark_ && "[Landmark::merge]| invalid landmark");
    assert(landmark_ != this && "[Landmark::merge]| same landmark, exit");

    //ia update the track - connect last SceneEntry of this
    //ia with the origin of landmark_
    SceneEntry* landmark_entry = landmark_->origin();
    SceneEntry* this_last_entry = _entry_ptr;

//    std::cerr << "dest                       = " << (size_t)this << std::endl;
//    std::cerr << "source                     = " << (size_t)landmark_ << std::endl;
//    std::cerr << "source->origin->landmark() = " << (size_t)landmark_entry->landmark() << std::endl;

    assert(landmark_entry->landmark() != this && "[Landmark::merge]| your bookkeeping is wrong, exit");
    assert(landmark_entry->landmark() == landmark_ && "[Landmark::merge]| your bookkeeping is wrong, exit");

    while (this_last_entry->nextSceneEntry()) {
      this_last_entry = this_last_entry->nextSceneEntry();
    }

    assert(this_last_entry->landmark() == this && "[Landmark::merge]| landmark ptr mismatch");

    //ia reassign edges
    for (g2o::HyperGraph::Edge* e : landmark_->edges()) {
      _edges.insert(e);
    }

    //ia update age
    const size_t& age_offset = this_last_entry->_age + 1;
    this_last_entry->_next = landmark_entry;
    landmark_entry->_age += age_offset;

    //ia adjust the bookkeeping
    while(landmark_entry) {
      landmark_entry->_age += age_offset;
      landmark_entry->setLandmarkPtr(this);
      landmark_entry = landmark_entry->nextSceneEntry();
    }

  }

	int Landmark::getDominantClassID()
	{
		// Initialize a map to store class incidences
		std::map<int, int> classVotes;

		// Iterate through all the observations of the landmark
		SceneEntry *sep = _origin;
		while (sep) {
			// If no semantic content is available, dodge
			if (! sep->hasValidSema()) {
				sep = sep->nextSceneEntry();
				continue;
			}

			// Get the semantic class
			int sds_index = sep->getSemaAssoc();
			int classID = sep->scene()->getSemantics().classIDs[sds_index];

			// Increment the stored counter
			classVotes[classID]++;	// Auto-initializes to 0 if classID wasn't in the map
			sep = sep->nextSceneEntry();
		}

		// Find class with most votes
		// NOTE: ties are allowed to break arbitrarily, they should be rare anyway,
		// however in case the std::map<> type doesn't store things deterministically
		// this might introduce a small degree of entropy.
		int maxVotes = -1, bestClass = -10;
		std::map<int, int>::iterator mit;
		for (mit = classVotes.begin(); mit != classVotes.end(); ++mit) {
			if (mit->second > maxVotes) {
				maxVotes = mit->second;
				bestClass = mit->first;
			}
		}

		// WARNING: this can still be trash in case no scene entries have
		// usable semantics!
		return bestClass;
	}

	Vector3 Landmark::getMedPosition()
	{
		// Iterate over all global matchables in the landmark track
		std::vector<Vector3> acc;
		SceneEntry *sep = _origin;
		while (sep) {
			acc.push_back(sep->globalMatchable().point);
			sep = sep->nextSceneEntry();
		}

		/**
		if (acc.size() >= 30) {
			for (auto v : acc) {
				std::cout << v << std::endl << std::endl;
			} throw;
		}
		**/

		// If nothing is here, something is wrong
		if (acc.size() == 0) {
			std::cerr
				<< "[Landmark]::getMedPosition() - Zero scene entries in this landmark, something is wrong!"
				<< std::endl;
			throw;
		}

		// Get vectors of each dimension independently
		std::vector<real> xd, yd, zd;
		for (size_t i=0; i<acc.size(); i++) {
			xd.push_back(acc[i][0]);
			yd.push_back(acc[i][1]);
			zd.push_back(acc[i][2]);
		}

		// Find the median for each dimension
		std::sort(xd.begin(), xd.end());
		std::sort(yd.begin(), yd.end());
		std::sort(zd.begin(), zd.end());
		Vector3 res(
				xd[xd.size()/2],
				yd[yd.size()/2],
				zd[zd.size()/2]);

		// Return the result
		// NOTE: each dimension is treated *independently*
		return res;
	}

	Vector3 Landmark::getAvgPosition()
	{
		Vector3 acc = Vector3::Zero();
		SceneEntry *sep = _origin;
		size_t cnt = 0;
		while (sep) {
			acc += sep->globalMatchable().point;
			cnt++;
			sep = sep->nextSceneEntry();
		}
		if (cnt > 0) {
			return acc / cnt;
		} else {
			std::cerr
				<< "[Landmark]::getAvgPosition - Zero scene entries in this landmark, something is wrong!"
				<< std::endl;
			throw;
		}
	}

} /* namespace srrg_bagasha */
