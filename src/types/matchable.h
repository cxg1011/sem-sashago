#pragma once
#include "defs.h"

namespace srrg_sashago {
  //! @brief base matchable. it only has a type,
  //!        which is constant, forever.
  struct MatchableBase {
    enum Type { Point = 0, Line = 1, Plane = 2 };

    inline const Type type() const {
      return _type;
    }

    MatchableBase(const Type type_ = Point):
      _type(type_) {
    }

  protected:
    Type _type;
  };

  //! @brief actual matchable class. this represents a mathemagical
  //!        type - like an Isometry3 or a float - and, thus, here
  //!        there are only stuff that can be useful at the mathematical
  //!        level. No drawing function, no parameters for the age
  //!        and other stuff like that.
  struct Matchable : public MatchableBase {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    Matchable(const Type type_ = MatchableBase::Point,
               const Vector3& point_ = Vector3::Zero(),
               const Matrix3& rotation_ = Matrix3::Identity()):
      MatchableBase(type_),
      point(point_),
      rotation(rotation_) {

      omega_p.setZero();
      switch (type_) {
        case MatchableBase::Type::Point:omega_p.setIdentity();
          break;
        case MatchableBase::Type::Line:omega_p.diagonal()[0] = _epsilon;
          omega_p.diagonal()[1] = 1;
          omega_p.diagonal()[2] = 1;
          break;
        case MatchableBase::Type::Plane:omega_p.diagonal()[0] = 1;
          omega_p.diagonal()[1] = _epsilon;
          omega_p.diagonal()[2] = _epsilon;
          break;
        default:break;
      }
    }

    Matchable(const Matchable& other_) :
      MatchableBase(other_._type),
      point(other_.point),
      rotation(other_.rotation),
      omega_p(other_.omega_p) {}

    //! @brief compute and set the rotation matrix from the direction of
    //!        the normal - computed as Rz*Rx*Ry
    inline void setRotationFromDirection(const Vector3& direction_) {
      real d = std::sqrt(direction_.x() * direction_.x() + direction_.y() * direction_.y());

      const real& dirx = direction_.x();
      const real& diry = direction_.y();
      const real& dirz = direction_.z();

      if (d > std::numeric_limits<real>::min()) {
        rotation <<
                  dirx, diry / d, dirx * dirz / d,
                  diry, -dirx / d, diry * dirz / d,
                  dirz, 0, -d;
      } else {
        rotation << 0, 1, 0,
                    0, 0, 1,
                    1, 0, 0;
      }

      omega_p.setZero();
      switch (_type) {
        case MatchableBase::Type::Point:omega_p.setIdentity();
          break;
        case MatchableBase::Type::Line:omega_p.diagonal()[0] = _epsilon;
          omega_p.diagonal()[1] = 1;
          omega_p.diagonal()[2] = 1;
          break;
        case MatchableBase::Type::Plane:omega_p.diagonal()[0] = 1;
          omega_p.diagonal()[1] = _epsilon;
          omega_p.diagonal()[2] = _epsilon;
          break;
        default:break;
      }
    }

    //! @brief direction vector is the 1st column of rotation matrix
    inline const Vector3 directionVector() const {
      return (Vector3)rotation.col(0);
    }

    inline void transformInPlace(const Isometry3& isometry_) {
      point=isometry_*point;
      if (_type==Point){
        return;
      }
      rotation = isometry_.linear()*rotation;
    }


    inline Matchable transform(const Isometry3& isometry_) const {
      Matchable new_m = Matchable(*this);
      new_m.transformInPlace(isometry_);

      return new_m;
    }

    friend std::ostream& operator<< (std::ostream& os_, const Matchable& matchable_) {
      os_ << "type     = " << matchable_.type() << std::endl
          << "point    = " << matchable_.point.transpose() << std::endl
          << "rotation = \n" << matchable_.rotation << std::endl;
      return os_;
    }
    
    Vector3 point;        //! @brief point xyz
    Matrix3 rotation;     //! @brief rotation matrix that describes normal direction
    Matrix3 omega_p;      //! @brief complementary matrix that shapes the error function
    //!        based on the matchable type

    static constexpr real _epsilon = 1e-6;  //! @brief avoids rank losses of matrix omega_p
  };


} //ia end namespace
