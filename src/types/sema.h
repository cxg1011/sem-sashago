#pragma once

#include <vector>
#include "defs.h"

using namespace std;
using namespace Eigen;

namespace matchables {

class SemanticMeasurement {
	public:
		// Getters
		srrg_sashago::Vector2 getBBCentroid();
		vector<srrg_sashago::Vector2> getBBFull();
		float getScore() { return score; }
		int getClass() { return detClass; }

		// Ctors - dtor
		SemanticMeasurement(int, float, vector<float>, vector<float>);
		SemanticMeasurement(int, float, vector<int>, vector<int>);
		~SemanticMeasurement();

		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
	private:
		// (x, y) image coordinates representing the top-left and bottom right points of the detected bounding box
		srrg_sashago::Vector2 p11, p22;

		// detection confidence score, provided directly from detector module
		float score;

		// detected class, see defs.h for details
		int detClass;
};

} // namespace matchables
