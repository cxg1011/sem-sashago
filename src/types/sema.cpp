#include "sema.h"

namespace matchables {

SemanticMeasurement::SemanticMeasurement(
		int detected_class,
		float classification_score,
		vector<float> coordinates_top_left,
		vector<float> coordinates_bot_right)
{
	detClass = detected_class;
	score = classification_score;

	p11(0) = coordinates_top_left[0];
	p11(1) = coordinates_top_left[1];
	p22(0) = coordinates_bot_right[0];
	p22(1) = coordinates_bot_right[1];
}

SemanticMeasurement::SemanticMeasurement(
		int detected_class,
		float classification_score,
		vector<int> coordinates_top_left,
		vector<int> coordinates_bot_right)
{
	vector<float>
		ctl(coordinates_top_left.begin(), coordinates_top_left.end()),
		ctr(coordinates_bot_right.begin(), coordinates_bot_right.end());
	SemanticMeasurement(
			detected_class,
			classification_score,
			ctl,
			ctr);
}

SemanticMeasurement::~SemanticMeasurement()
{
}

srrg_sashago::Vector2 SemanticMeasurement::getBBCentroid()
{
	float dx = p22(0) - p11(0);
	float dy = p22(1) - p11(1);
	srrg_sashago::Vector2 ret(p11(0) + dx/2, p11(1) + dy/2);
	return ret;
}

vector<srrg_sashago::Vector2> SemanticMeasurement::getBBFull()
{
	vector<srrg_sashago::Vector2> ret(4);
	ret.emplace_back(p11(0), p11(1));
	ret.emplace_back(p11(0), p22(1));
	ret.emplace_back(p22(0), p11(1));
	ret.emplace_back(p22(0), p22(1));
	return ret;
}

} // namespace matchables
