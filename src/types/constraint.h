#pragma once
#include "scene_entry.h"

namespace srrg_sashago {
  // This type represents a constraint between two matchables
  // all constraints are assumed to be of containment type
  // unless is_intersection is specified
  struct Constraint {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    SceneEntry* fixed = 0;
    SceneEntry* moving = 0;
    bool is_intersection; //< true if the constraint is an intersection between lines
    int weight = 1; //< the omega will be multiplied by this weight
    real error_p; //< the solver writes here the point error at each round
    real error_d; //< the solver writes here the directon error at each round
    real error_o; //< the solver writes here the orthogonality error at each round
    real error_x; //< the solver writes here the intersection error at each round

    Constraint(SceneEntry* fixed_ = 0,
               SceneEntry* moving_ = 0,
               bool is_intersection_ = false) {
      fixed = fixed_;
      moving = moving_;
      is_intersection = is_intersection_;
      weight = 1;
      resetErrors();
    }
    void resetErrors() {
      error_p = 0;
      error_d = 0;
      error_o = 0;
      error_x = 0;
    }
  };

  using ConstraintVector = std::vector<Constraint, Eigen::aligned_allocator<Constraint> >;
} //ia end namespace
