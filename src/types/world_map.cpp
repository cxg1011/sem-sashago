#include "world_map.h"

namespace srrg_sashago {

  size_t WorldMap::landmark_id_generator = 0;

  WorldMap::WorldMap() {}

  WorldMap::~WorldMap() {
    destroy();
  }

  void WorldMap::destroy() {
    if (!_landmarks.size()) {
      return;
    }

    LandmarkContainer::iterator l_it = _landmarks.begin();
    LandmarkContainer::iterator l_it_end = _landmarks.end();

    while (l_it != l_it_end) {
      delete *l_it;
      ++l_it;
    }

    _landmarks.clear();
  }

  Landmark* WorldMap::addEntry(SceneEntry* entry_) {
    Landmark* landmark = 0;

    if (!entry_->prevSceneEntry()) {
      _entry_pool.push_back(entry_);
      return landmark;
    }

    if (entry_->landmark()) {
      //ia landmark update
      landmark = entry_->landmark();
    } else {
      if (_isOldEnough(entry_)) {
        //ia create a new Landmark
        landmark = new Landmark(entry_, landmark_id_generator++);
        _landmarks.insert(landmark);
      } else {
        _entry_pool.push_back(entry_);
      }
    }

    return landmark;
  }


  void WorldMap::mergeLandmarks(Landmark* dest_,
                                Landmark* source_,
                                bool erase_) {
    assert(dest_ != source_ && "[WorldMap::mergeLandmarks]| merging the same landmark");
    dest_->merge(source_);

    LandmarkContainer::iterator l_it = _landmarks.find(source_);
    if (l_it == _landmarks.end()) {
      throw std::runtime_error("[WorldMap::mergeLandmarks]| cannot find the damn landmark");
      return;
    }

    _landmarks.erase(l_it);
    delete source_;
  }


  void WorldMap::updatePool() {
    LandmarkContainer::const_iterator l_it = _landmarks.begin();
    LandmarkContainer::const_iterator l_end = _landmarks.end();
    size_t k = _entry_pool.size();
    _entry_pool.resize(_entry_pool.size() + _landmarks.size());

    while (l_it != l_end) {
      _entry_pool[k++] = (*l_it)->entry();
      ++l_it;
    }
  }


  void WorldMap::clearPool() {
    _entry_pool.clear();
  }


  const bool WorldMap::_isOldEnough(SceneEntry* e_) const {

    int threshold = -1;
    switch(e_->matchable().type()) {
    case MatchableBase::Type::Point:
      threshold = _configuration.age_threshold_point;
      break;
    case MatchableBase::Type::Line:
      threshold = _configuration.age_threshold_line;
      break;
    case MatchableBase::Type::Plane:
      threshold = _configuration.age_threshold_plane;
      break;
    default:
      throw std::runtime_error("[WorldMap::isOldEnough]| unknown matchable type");
    }

    return e_->age() >= threshold;
  }

} //ia end namespace srrg_bagasha
