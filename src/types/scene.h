#pragma once
#include <g2o/core/optimizable_graph.h>
#include <atomic>
#include "scene_entry.h"

#include "detector/semantic_detector.h"

namespace srrg_sashago {

  //! @brief a scene is a collection of matchables extracted directly from the image
  //!        the scene owns the matchables and stores also additional information
  //!        like the pose relative to that scene, time_stamp and epoch.
  class Scene {
  public:
    //! @brief ctor - dtor
    Scene();
    virtual ~Scene();

    //! @brief clears the scene and deletes the matchables
    //!        if @destroy_ flag is true, frees also the memory
    void clear(const bool& destroy_ = true);

    //! @brief inline get methods
    inline const SceneEntryVector& entries() const {return _scene_entries;}
    inline const Isometry3& pose() const {return _pose;}
    inline const Isometry3& gtPose() const {return _gt_pose;}
    inline g2o::HyperGraph::Vertex* poseVertex() const {return _pose_vertex;}
    inline const double& timeStamp() const {return _timestamp;}
    inline const size_t& sceneID() const {return _id;}
    inline const bool isSet() const {return _is_pose_set;}

    //! @brief inline set methods
    inline void setPoseVertex(g2o::HyperGraph::Vertex* pose_vertex_) {_pose_vertex = pose_vertex_;}
    inline void setTimestamp(const double& timestamp_) {_timestamp = timestamp_;}
    inline void setGTPose(const Isometry3& gt_) {_gt_pose = gt_;}

    //! @brief sets the pose of the scene and triggers the compututation
    //!        of the global matchable for all its entries
    void setPose(const Isometry3& pose_);

    //! @brief create a new entry and stores it in the vector.
    //!        The created item is returned.
    SceneEntry* addEntry(const Matchable& matchable_);

	void setSemantics(const SemaDetectionSet sds_in) { _sds = sds_in; }
	SemaDetectionSet getSemantics() { return _sds; }
	// Associate the semantic measurements to the SceneEntries (?) in this Scene
	void assocSemantics();

	void integrateSemantics(Scene*, float dist_thresh=150);
	inline std::vector<int> getSemaLinks() { return _sema_links; }

	bool hasValidSemantics();

    //! @brief resets the ID generator
    static void resetIdGenerator() {
      _id_generator = 0;
    }

  protected:
    SceneEntryVector _scene_entries;             //ia collection of extracted scene entries - OWNED
    Isometry3 _pose;                             //ia pose of the camera
    Isometry3 _gt_pose;                          //ia pose of the GT if defined - default is identity
    g2o::HyperGraph::Vertex* _pose_vertex = 0;   //ia ptr to the pose vertex in the graph
    double _timestamp;                           //ia timestamp of the image
    size_t _id;                                  //ia just in case

    bool _is_pose_set;

	/** Semantic variables **/
	SemaDetectionSet _sds;
	
	// _sema_links[i] = r, where <some_scene>.getSemantics()[r] returns the closest
	// semantic detection to _sds[i]
	std::vector<int> _sema_links;

  private:
    static uint64_t _id_generator;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

  using SceneVector = std::vector<Scene*, Eigen::aligned_allocator<Scene*> >;
  using IntScenePair = std::pair<uint64_t, Scene*>;
  using IntSceneMap = std::map<uint64_t, Scene*, std::less<uint64_t>, Eigen::aligned_allocator<std::pair<uint64_t, Scene*> > >;

} /* namespace srrg_bagasha */

