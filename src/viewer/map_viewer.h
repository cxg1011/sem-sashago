#pragma once
#include <atomic>
#include <QtWidgets/qapplication.h>
#include <QtWidgets/QWidget>
#include <QtGui/QKeyEvent>
#include <QGLViewer/qglviewer.h>

#include <srrg_core_viewers/simple_viewer.h>

#include "types/scene.h"
#include "types/world_map.h"

#include <mutex>

#include <vector>
#include <fstream>
#include <string>

namespace srrg_sashago {

  //! @brief modified camera, that suits our needs
  class StandardCamera : public qglviewer::Camera {
  public:
    StandardCamera(): _standard(true) {}

    qreal zNear() const {
      if(_standard) { return qreal(_z_near); }
      else { return Camera::zNear(); }
    }

    qreal zFar() const {
      if(_standard) { return qreal(_z_far); }
      else { return Camera::zFar(); }
    }

    bool standard() const { return _standard; }
    void setStandard(bool s) { _standard = s; }
    void setZNear(const float& z_near_) {_z_near = z_near_;}
    void setZFar(const float& z_far_) {_z_far = z_far_;}

  protected:
    bool _standard;
    float _z_near = 0.001f;
    float _z_far  = 10000.0f;
	std::vector<Eigen::Vector3f> _sema_colors;
  };

  //! @brief actual viewer, derived from qglviewer, srrg1 style
  class MapViewer : public QGLViewer {
  public:

    struct Config {
      VerbosityLevel verbosity;

      float point_size;
      float line_width;

      float camera_wf_height;
      float camera_wf_width;
      Eigen::Vector3f camera_wf_color;
      Eigen::Vector3f gt_color;

      size_t camera_track_distance;

      bool draw_gt;
      bool draw_trajectory;
      bool draw_simple_planes;
      bool draw_landmarks;
      bool draw_full_camera_track;
      bool draw_last_scene;
      bool follow_camera;

	  std::string color_fname;

      Config() {
        verbosity = VerbosityLevel::Info;

        point_size = 2.0f;
        line_width = 2.0f;
        camera_wf_height = 0.1f;
        camera_wf_width = 0.05f;
        camera_wf_color = VIEWER_COLOR_DARK_CYAN;
        gt_color = VIEWER_COLOR_VIOLET;
        camera_track_distance = 35;
        draw_gt = false;
        draw_trajectory = false;
        draw_simple_planes = false;
        draw_landmarks = true;
        draw_full_camera_track = false;
        draw_last_scene = false;
        follow_camera = false;
	color_fname = "/path/to/srrg_sashago/configurations/colors.txt";
      }
    };

    MapViewer(QWidget* parent_ = 0);
    virtual ~MapViewer();

    //! @brief inline set-get methods
    inline const Config& config() const {return _configuration;}
    inline Config& mutableConfig() {return _configuration;}

    inline StandardCamera* camera() const {return _camera;}
    inline const bool terminationRequested() const {return _termination_requested;}
    inline const bool pausePlayback() const {return _pause_playback;}
    inline const bool stepMode() const {return _step_mode;}
    inline const uint64_t numSteps() const {return _num_steps;}

    inline void setMachableMap(WorldMap* map_) {_map = map_;}
    inline void setSceneContainer(IntSceneMap* scenes_) {_scenes = scenes_;}
    inline void setQServer(QApplication* qapp_) {_qapp_ptr = qapp_;}
    inline const bool isActive() const {return isVisible();}

    inline void lock() {_mtx_data_exchange.lock();}
    inline void unlock() {_mtx_data_exchange.unlock();}

    //! @brief decrements by 1 the number of steps to process - if in step mode
    inline void decrementStep() {--_num_steps;}

    //! @brief qglviewer function to initialize the things
    void init() override;

    //! @brief qglviewer function that actually draws the things
    void draw() override;

    void showViewport() {show();}

    //! @brief check if it's visible, updates gl
    void update();

    //! @brief qglviewer function that handles keypresses
    void keyPressEvent(QKeyEvent* event_) override;

    //! @brief help string
    QString helpString() const override;

  protected:
    //! @brief aux functions
    void _drawMapLandmarks() const;
    void _drawDenseCameraTrack() const;
    void _drawSparseCameraTrack() const;
    void _drawCameraTrack() const;
    void _drawGTCameraTrack() const;
    void _drawScene(Scene* scene_) const;
    void _drawCamera(const Isometry3& transform_) const;
    void _drawSceneEntry(SceneEntry* entry_, const Eigen::Vector3f& color_rgb_) const;

    void _drawPoint(const Vector3& p_, const Eigen::Vector3f& color_rgb_)  const;
    void _drawLine(const Vector3& p0_,
                   const Vector3& p1_,
                   const Eigen::Vector3f& color_rgb_) const;
    void _drawSimplePlane(const Vector3& center_,
                          const Matrix3& rotation_,
                          const Eigen::Vector3f& color_rgb_) const;
    void _drawPlane(const srrg_core::Cloud3D& plane_cloud_,
                    const Vector3& center_,
                    const Matrix3& rotation_,
                    const Eigen::Vector3f& color_rgb_) const;

    const Eigen::Vector3f _computeMatchableColor(SceneEntry* entry_) const;

    //! @brief attributes
    StandardCamera* _camera = 0;
    Config _configuration;

    //! @brief pointer to the map - that is the only thing that we want to see.
    WorldMap* _map = 0;

    //! @brief pointer to the scenes - to draw the camera track
    IntSceneMap* _scenes = 0;

    //! @brief signals read from keyboard
    std::atomic<bool> _termination_requested;
    std::atomic<bool> _pause_playback;
    std::atomic<bool> _step_mode;

    //! @brief number of steps to perform in step mode
    std::atomic<uint64_t> _num_steps;
    
    static Matrix3 _plane_rotation_offset;
    static Isometry3 _camera_offset;

    QApplication* _qapp_ptr = 0;

    std::mutex _mtx_data_exchange;

	// Semantics
	void _readSemanticColorMap(std::string);
	std::vector<Eigen::Vector3f> _sema_colors;
  };

} /* namespace srrg_bagasha */

