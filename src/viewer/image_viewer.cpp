#include <viewer/image_viewer.h>

namespace srrg_sashago {

  ImageViewer::ImageViewer() {
    _tuning_mode = false;
  }

  ImageViewer::~ImageViewer() {}

  void ImageViewer::draw() {
    if (!_current_scene)
      throw std::runtime_error("[BagashaImageViewer::draw]| no scene to draw, quit");
    if (_current_image.empty())
      throw std::runtime_error("[BagashaImageViewer::draw]| no image to draw, quit");


    _mtx_data_exchange.lock();
    _drawSceneEntries(_current_scene->entries(), _current_image, _plane_roi_image);
    _mtx_data_exchange.unlock();
    //ia once that we accumulated all the plane drawing, do the blending if needed
    if (_config.draw_full_plane) {
      addWeighted( _current_image, _config.alpha, _plane_roi_image, 1.0-_config.alpha, 0.0, _current_image);
    }

    if(!_tuning_mode) {
      cv::Mat detection_and_depth = _current_image;
      if(_depth_image.rows == detection_and_depth.rows) {
        cv::imshow("current depth", _depth_image*5);        
        //        cv::hconcat(_current_image, _depth_image, detection_and_depth);
      }
      _mtx_data_exchange.lock();
      cv::imshow(_config.window_name.c_str(), _current_image);     
      cv::waitKey(1);
      _mtx_data_exchange.unlock();
      return;
    } else {
      if(_matching_scene) {
        _drawSceneEntries(_matching_scene->entries(), _matching_image, _matching_plane_roi_image);
        if (_config.draw_full_plane) {
          addWeighted( _matching_image, _config.alpha, _matching_plane_roi_image, 1.0-_config.alpha, 0.0, _matching_image);
        }
      } else {
        _matching_image.create(_current_image.size(), _current_image.type());
        _matching_image = 0;
        _matching_plane_roi_image.create(_plane_roi_image.size(), _plane_roi_image.type());
        _matching_plane_roi_image = 0;
      }
      //bdc ptr copy
      cv::Mat concatenated_image = _current_image;
      cv::vconcat(_current_image, _matching_image, concatenated_image);
      _drawCorrespondences(concatenated_image);
      //ia show the image      
      cv::imshow(_config.window_name.c_str(), concatenated_image);
      cv::waitKey(1);
    }
  }


  void ImageViewer::_drawSceneEntries(const SceneEntryVector& entries_,
                                             cv::Mat& image_,
                                             cv::Mat& plane_roi_image_) {
    for (size_t i = 0; i < entries_.size(); ++i) {
      SceneEntry* entry = entries_[i];

      switch (entry->matchable().type()) {
      case Matchable::Type::Point:
        {
          _drawPoint(entry, image_);
          break;
        }
      case Matchable::Type::Line:
        {
          _drawLine(entry, image_);
          break;
        }
      case Matchable::Type::Plane:
        {
          if (!_tuning_mode)
            _drawPlane(entry, image_, plane_roi_image_);
          break;
        }
      default:
        throw std::runtime_error("[BagashaImageViewer::draw]| unknown matchable type, quit");
      }
    }
  }

  
  void ImageViewer::_drawPoint(SceneEntry* entry_, cv::Mat& image_) {
    //ia reproject the point
    cv::Point reprojected_point = _projectCentroid(entry_->matchable().point);

    //ia select a color - if its a landmark, than DARK_RED, YELLOW otherwise
    cv::Scalar color = CV_COLOR_CODE_YELLOW;
    if (entry_->landmark())
      color = CV_COLOR_CODE_RED;

    if (_tuning_mode) {
      color = CV_COLOR_CODE_RED;
    }

    //ia draw the reprojected point
    cv::circle(image_, reprojected_point, _config.point_radius, color);

    //ia draw age (aka track length)
    cv::putText(image_, std::to_string(entry_->age()), reprojected_point+cv::Point2i(5, 5), cv::FONT_HERSHEY_SCRIPT_SIMPLEX, 0.25, CV_COLOR_CODE_RED);
  }

  void ImageViewer::_drawLine(SceneEntry* entry_, cv::Mat& image_) {
    //ia get the end point of the line
    const Vector3& direction_vector = entry_->matchable().rotation.col(0);
    Vector3 end_point = entry_->matchable().point + direction_vector*entry_->extent().x();

    //ia reproject the points
    cv::Point starting_point = _projectCentroid(entry_->matchable().point);
    cv::Point ending_point = _projectCentroid(end_point);

    //ia select a color - if its a landmark, than GREEN, YELLOW otherwise
    cv::Scalar color = CV_COLOR_CODE_YELLOW;
    if (entry_->landmark())
      color = CV_COLOR_CODE_GREEN;

    if (_tuning_mode) {
      color = CV_COLOR_CODE_BLUE;
    }

    //ia draw the reprojected line
    cv::line(image_, starting_point, ending_point, color, _config.line_width);

    //ia draw age (aka track length)
    cv::putText(image_, std::to_string(entry_->age()), starting_point+cv::Point2i(5, 5), cv::FONT_HERSHEY_SCRIPT_SIMPLEX, 0.25, CV_COLOR_CODE_RED);
  }

  void ImageViewer::_drawPlane(SceneEntry* entry_, cv::Mat& image_, cv::Mat& plane_roi_image_) {
    //ia get a color
    cv::Scalar color = CV_COLOR_CODE_YELLOW;
    if (entry_->landmark())
      color = CV_COLOR_CODE_BLUE;

    //ia reproject the cloud centroid
    cv::Point centroid_point = _projectCentroid(entry_->matchable().point);
    //ia draw the centroid
    cv::circle(image_, centroid_point, _config.point_radius*2, color, cv::FILLED);
//    cv::drawMarker(_current_image, centroid_point, color, cv::MARKER_TILTED_CROSS, _config.point_radius*3);

    //ia draw age (aka track length)
    cv::putText(image_, std::to_string(entry_->age()), centroid_point+cv::Point2i(5, 5), cv::FONT_HERSHEY_SCRIPT_SIMPLEX, 0.25, CV_COLOR_CODE_RED);

    //ia if we do not want to draw the full plane but only the centroid, then stop here
    if (!_config.draw_full_plane)
      return;

    //ia matemagical mega-trick
    //ia this matrix trasforms the matchable rotation matrix (Rzxy) into
    //ia a normal rotatation Rxyz. Otherwise the plane will be wrongly shown
    Matrix3 r_xyz = Matrix3::Zero();
    r_xyz << 0,0,1,1,0,0,0,1,0;

    //ia reproject the contour cloud
    std::vector<cv::Point> reprojected_contours(entry_->contourCloud().size(), cv::Point(0,0));
    //ia cloud is not in the right RF - each point is relative to the plane centroid
    Isometry3 T_matchable = Isometry3::Identity();
    T_matchable.linear() = entry_->matchable().rotation*r_xyz;
    T_matchable.translation() = entry_->matchable().point;

    for (size_t i = 0; i < entry_->contourCloud().size(); ++i) {
      //ia reprojected tranformed point
      reprojected_contours[i] = _projectCentroid(T_matchable*entry_->contourCloud()[i].point());
    }

    //ia draw the contour on the auxiliary image (than we'll do the blend between this and the original image)
    cv::fillConvexPoly(plane_roi_image_, &reprojected_contours[0], reprojected_contours.size(), color, 8, 0);
  }

  void ImageViewer::_drawCorrespondences(cv::Mat& image_) {
    if(!_constraints)
      return;
    const size_t cols = image_.cols;
    const size_t rows = image_.rows;
    //bdc given a vertically concatenated image, so this is the shift
    const size_t shift = rows / 2;
    
    for(const Constraint constraint : *_constraints) {
      SceneEntry* quiry_constraints = constraint.moving; // upper image
      SceneEntry* reference_constraints = constraint.fixed; // lower image
      
      cv::Point quiry_point = _projectCentroid(quiry_constraints->matchable().point);
      cv::Point reference_point = _projectCentroid(reference_constraints->matchable().point);
      reference_point.y += shift;

      cv::Scalar color;
      switch (quiry_constraints->matchable().type()) {
        case Matchable::Type::Point:
          color = CV_COLOR_CODE_RED;
          break;
        case Matchable::Type::Line:
          color = CV_COLOR_CODE_DARKGREEN;
          break;
        default:
          throw std::runtime_error("[BagashaImageViewer::drawCorrespondences]| invalid matchable type");
      }

      if (quiry_constraints->matchable().type() != reference_constraints->matchable().type()) {
        color = CV_COLOR_CODE_VIOLETT;
        std::cerr << "[BagashaImageViewer::drawCorrespondences]| match between different constraint types" << std::endl;
      }

      cv::line(image_, quiry_point, reference_point, color, 1, cv::LINE_AA);
    }
    
  }

  cv::Point ImageViewer::_projectCentroid(const Vector3& centroid_) {
    Vector3 projected_centroid = _K * centroid_;
    projected_centroid /= projected_centroid.z();
    return cv::Point(projected_centroid.x(), projected_centroid.y());
  }
  
} /* namespace srrg_bagasha */
