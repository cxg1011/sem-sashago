#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/version.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/line_descriptor.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
// #include <opencv2/xfeatures2d.hpp>

#include <srrg_types/cloud_3d.h>
#include <srrg_image_utils/depth_utils.h>
#include <srrg_image_utils/point_image_utils.h>
#include <srrg_system_utils/system_utils.h>
#include <srrg_path_map/clusterer_path_search.h>

#include "types/scene.h"
#include "types/constraint.h"
#include "types/world_map.h"

#include <mutex>

namespace srrg_sashago {


  class ImageViewer {
  public:

    struct Config {
      size_t point_radius;
      size_t line_width;
      std::string window_name;
      float alpha;
      bool draw_full_plane;
      
      Config() {
        point_radius = 4;
        line_width = 2;
        window_name = "rgb image - current detection";
        draw_full_plane = true;
        alpha = 0.75;
      }
    };

    ImageViewer();
    virtual ~ImageViewer();

    //! @brief inline setup
    inline Config& mutableConfig() {return _config;}
    inline const Config& config() const {return _config;}

    inline void lock() {_mtx_data_exchange.lock();}
    inline void unlock() {_mtx_data_exchange.unlock();}

    inline void setCurrentImage(const cv::Mat& current_frame_,
                                const cv::Mat depth_image_ = cv::Mat()) {
      current_frame_.copyTo(_current_image);
      if (_config.draw_full_plane)
        _current_image.copyTo(_plane_roi_image);
      _depth_image = depth_image_; //ptr cpy (see cv doc before complain)
    }
    inline void setCurrentScene(Scene* scene_) {_current_scene = scene_;}
    inline void setCameraMatrix(const Eigen::Matrix3f camera_matrix_) {_K = camera_matrix_;}

    inline void setMatchingImage(const cv::Mat& matching_frame_) {
      matching_frame_.copyTo(_matching_image);
      if (_config.draw_full_plane)
        _matching_image.copyTo(_matching_plane_roi_image);
    }
    inline void setMatchingScene(Scene* scene_, ConstraintVector* constraints_ = NULL) {
      _matching_scene = scene_;
      _constraints = constraints_;
    }    
    inline void setTuningMode(const bool& tuning_mode_flag_) {
      _tuning_mode = tuning_mode_flag_;
    }
    
    //! @brief draw things on the image and show it
    void draw();

  protected:
    //! @brief helper functions
    void _drawSceneEntries(const SceneEntryVector& entries_,
                           cv::Mat& image_,
                           cv::Mat& plane_roi_image);
    void _drawPoint(SceneEntry* entry_, cv::Mat& image_);
    void _drawLine(SceneEntry* entry_, cv::Mat& image_);
    void _drawPlane(SceneEntry* entry_, cv::Mat& image_, cv::Mat& plane_image_roi_);
    void _drawCorrespondences(cv::Mat& image_);
    cv::Point _projectCentroid(const Vector3& centroid_);
    
    //! @brief attributes
    cv::Mat _current_image;
    cv::Mat _plane_roi_image;
    cv::Mat _depth_image;
    Scene*  _current_scene = 0;
    Eigen::Matrix3f _K;

    //! @brief hbst tuning things
    cv::Mat _matching_image;
    cv::Mat _matching_plane_roi_image;
    Scene*  _matching_scene        = 0;
    ConstraintVector* _constraints = 0;
    bool _tuning_mode;
    
    //! @brief ugly parameters
    Config _config;

    std::mutex _mtx_data_exchange;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

  //! @brief cv colors definitions
  #define CV_COLOR_CODE_RANDOM cv::Scalar(rand()%255, rand()%255, rand()%255)
  #define CV_COLOR_CODE_GREEN cv::Scalar(0, 200, 0)
  #define CV_COLOR_CODE_BLUE cv::Scalar(255, 0, 0)
  #define CV_COLOR_CODE_DARKBLUE cv::Scalar(150, 0, 0)
  #define CV_COLOR_CODE_RED cv::Scalar(0, 0, 255)
  #define CV_COLOR_CODE_DARKRED cv::Scalar(0, 0, 190)
  #define CV_COLOR_CODE_YELLOW cv::Scalar(0, 255, 255)
  #define CV_COLOR_CODE_WHITE cv::Scalar(255, 255, 255)
  #define CV_COLOR_CODE_BLACK cv::Scalar(0, 0, 0)
  #define CV_COLOR_CODE_ORANGE cv::Scalar(0, 150, 255)
  #define CV_COLOR_CODE_DARKGREEN cv::Scalar(0, 150, 0)
  #define CV_COLOR_CODE_BROWN cv::Scalar(0, 100, 175)
  #define CV_COLOR_CODE_VIOLETT cv::Scalar(255, 0, 255)
  #define CV_COLOR_CODE_DARKVIOLETT cv::Scalar(150, 0, 150)


} //ia namespace srrg_bagasha

