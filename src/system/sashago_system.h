#pragma once
#include <detector/shape_detector.h>
#include <loop_closer/loop_closer.h>
#include <relocalizer/relocalizer.h>
#include <tracker/shape_tracker.h>
#include <viewer/image_viewer.h>
#include <viewer/map_viewer.h>
#include "txtio_parser.h"
#include "yaml_parser.h"

namespace srrg_sashago {

  class YamlParser;
  
  class SashagoSystem {
  public:

    using SceneContainer = IntSceneMap;

    struct Config {
      VerbosityLevel verbosity;
      std::string filename_output_track_standard;
      std::string filename_output_track_kitti;
      std::string filename_output_track_txtio;
      bool use_guess;
      bool use_gui;
      bool save_open_loop_traj;
      bool save_final_traj;
      bool save_graph;
      bool save_track_kitti;
      bool save_track_tum;
      bool save_track_txtio;

      Config() {
        verbosity = VerbosityLevel::Info;
        filename_output_track_standard = "track_standard.bagasha";
        filename_output_track_kitti    = "track_kitti.bagasha";
        filename_output_track_txtio    = "track_txtio_bagasha.txtio";
        use_guess = false;
        use_gui = false;

        save_open_loop_traj = true;
        save_final_traj = true;
        save_graph = true;
        save_track_kitti = true;
        save_track_tum = true;
        save_track_txtio = false;
      }
    };

    struct TimeStats {
      double detection;
      double tracking;
      double relocalization;

      inline void setZero() {
        detection = 0.0;
        tracking = 0.0;
        relocalization = 0.0;
      }

      TimeStats() {
        setZero();
      }

      TimeStats& operator+= (const TimeStats& time_stats) {
        detection += time_stats.detection;
        tracking += time_stats.tracking;
        relocalization += time_stats.relocalization;
        return *this;
      }

      TimeStats operator/ (const real& d) {
        TimeStats mean;
        mean.detection = detection/d;
        mean.tracking = tracking/d;
        mean.relocalization = relocalization/d;
        return mean;
      }

      friend std::ostream& operator<< (std::ostream& os_, const TimeStats& stats_) {
        os_ << std::setprecision(4);
        os_ << "[BagashaSystem::TimeStats]| detection=" << stats_.detection
            << "  \ttracking=" << stats_.tracking
            << "  \trelocalization=" << stats_.relocalization;
        return os_;
      }
    };


    SashagoSystem();
    virtual ~SashagoSystem();

    //! @brief inline getter methods for the modules - non const to do the configuration
    inline ShapeDetector* detector() const {return _detector;}
    inline ShapeTracker* tracker() const {return _tracker;}
    inline Relocalizer* relocalizer() const {return _relocalizer;}
    inline LoopCloser* loopCloser() const {return _loop_closer;}
    inline MapViewer* mapViewer() const {return _map_viewer;}
    inline ImageViewer* imageViewer() const {return _image_viewer;}

    //! @brief inline getters
    inline IntSceneMap* scenes() const {return _scenes;}
    inline WorldMap* map() const {return _map;}

    //! @brief configuration
    inline Config& mutableConfig() {return _config;}
    inline const Config& config() const {return _config;}

    //! @brief sets a qgl server and initializes the viewer - for gui
    void setupGUI(QApplication* qapp_);

    //! @brief updates the GL loop, process events and stuff like this
    void updateGUI();

    //! @brief load parameters from yaml file
    void loadConfigurationFromFile(const std::string& yaml_filename_);

    //! @brief writes paramters to yaml file
    void writeConfigurationToFile(const std::string& yaml_filename_);

    //! @brief process a txtio dataset - it configures also the parser from configuration file
    void processDataFromFile(const std::string& filename_);

    //! @brief process a txtio dataset - it configures also the parser from configuration file
    //!        processing is done in another thread. Returns the handler to processing thread
    std::shared_ptr<std::thread> processDataFromFileInThread(const std::string& filename);

    //! @brief initialization of the system
    void init();

    //! @brief save the graph in g2o format
    void saveGraph(const std::string& filename_);
    
    //! @brief performs offline optimization of final graph - super shitty
    void optimizeGraph();

    //! @brief save the final track using the TUM format - <ts x y z qx qy qz qw>
    //! @brief param[in] overwrite: if true overwrites the file if already exists
    void saveOutputTrackStandard(const bool& overwrite_ = true);

    //! @brief save the final track using the KITTI format - <t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11>
    //! @brief param[in] overwrite: if true overwrites the file if already exists
    void saveOutputTrackKitti(const bool& overwrite_ = true);

    //! @brief save the final track using the TXTIO format 
    void saveOutputTrackTxtio();
    
    //! @brief computes a new scene from the fresh provided images
    void setImages(const srrg_core::RGBImage& rgb_image_,
                   const srrg_core::RawDepthImage& depth_image_,
                   const double& rgb_msg_timestamp_,
                   const Isometry3& gt_pose_ = Isometry3::Identity());

    //! @brief does the magic
    void compute();

  protected: //! @brief slam system modules

    //! @brief owned modules - these should go in the config
    ShapeDetector*      _detector = 0;
    ShapeTracker*       _tracker = 0;
    Relocalizer*   _relocalizer = 0;
    LoopCloser*    _loop_closer = 0;

    //! @brief matchable map - former MatchableStore - owned for the moment
    WorldMap*    _map = 0;

    //! @brief scene container - scenes are owned for the moment
    IntSceneMap* _scenes = 0;

    //! @brief ptr of the current images
    srrg_core::RGBImage      _current_rgb;
    srrg_core::RawDepthImage _current_depth;
    double  _rgb_timestamp;

    //! @brief current frame gt from dataset
    Isometry3 _current_gt_pose;

    //! @brief configuration
    Config _config;
    TimeStats _stats;
    TimeStats _cumulative_time_stats;

  protected: //! @brief dataset processing from txtio file
    TxtioParser* _txtio_parser = 0;

  protected: //! @brief parses parameters/configurations from yaml file
    YamlParser* _yaml_parser   = 0;
    
  protected: //! @brief opengl visualization
    MapViewer* _map_viewer = 0;
    QApplication* _qapp_ptr    = 0;

  protected: //! @brief opencv image visualization
    ImageViewer* _image_viewer = 0;
	Scene* _last_scene = 0;

  protected:
    //! @brief auxiliary function due to ugly parameters
    inline void _setVerbosityToAllModule() {
      _detector->mutableConfig().verbosity = _config.verbosity;
      _tracker->mutableConfig().verbosity = _config.verbosity;
      _tracker->aligner()->mutableConfig().verbosity = _config.verbosity;
      _tracker->aligner()->nnCorrespondenceFinder()->mutableConfig().verbosity = _config.verbosity;
      _relocalizer->mutableConfig().verbosity = _config.verbosity;
      _loop_closer->mutableConfig().verbosity = _config.verbosity;
      _txtio_parser->mutableConfig().verbosity = _config.verbosity;
      if (_map_viewer)
        _map_viewer->mutableConfig().verbosity = _config.verbosity;
    }
    
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };
  
} //ia end namespace

