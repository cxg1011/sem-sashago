#pragma once
#include <thread>

#include <srrg_messages/message_reader.h>
#include <srrg_messages/message_writer.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_messages/sensor_message_sorter.h>
#include <srrg_messages/message_timestamp_synchronizer.h>

#include "types/defs.h"
#include "types/scene.h"

namespace srrg_sashago {

  class TxtioParser {
  public:

    enum ParserStatus {Invalid=0x00,Unsync=0x01,Good=0x02,Stop=0xFF};

    struct Config {
      VerbosityLevel verbosity;

      std::string topic_depth;
      std::string topic_rgb;
      std::string dataset_filename;
      std::string output_filename;
      
      double syncronizer_timestep;
	  double depth_scale;

      Config() {
        verbosity = VerbosityLevel::Info;

        topic_depth      = "/depth";
        topic_rgb        = "/rgb";
        dataset_filename = "";
        output_filename  = "bagasha_corrected_poses.txtio"; //! overwrite the odometry
		depth_scale = 1000.0;
        
        syncronizer_timestep = 0.03;
      }
    };

    TxtioParser();
    virtual ~TxtioParser();

    //! @brief inline set/get methods
    inline const Config& config() const {return _config;}
    inline Config& mutableConfig() {return _config;}

    inline srrg_core::MessageReader* reader() const {return _reader;}
    inline srrg_core::MessageWriter* writer() const {return _writer;}
    inline srrg_core::MessageTimestampSynchronizer* synchronizer() const {return _synchronizer;}

    inline const size_t& messageNumber() const {return _message_number;}
    inline const Eigen::Matrix3f& cameraMatrix() const {return _camera_matrix;}
    inline const srrg_core::RawDepthImage& imageDepth() const {return _image_depth;}
    inline const srrg_core::RGBImage& imageRGB() const {return _image_rgb;}
    inline const double& timestamp() const {return _timestamp;}
    inline const Eigen::Isometry3f& gt() const {return _gt_pose;}
    inline const Eigen::Isometry3f& cameraGuess() const {return _camera_guess;}

    //! @brief initializes things and checks that everything is well formed
    void init();

    //! @brief parses things and generates images - this will go on a standalone thread
    const ParserStatus processData();

    //! @brief write in batch the txtio data w/ the provided poses
    void write(IntSceneMap* scenes_);
    
  protected:
    //! @brief reader/writer and syncronizer modules - owned
    srrg_core::MessageReader* _reader = 0;
    srrg_core::MessageWriter* _writer = 0;
    srrg_core::MessageTimestampSynchronizer* _synchronizer = 0;

    //! @brief the actual images to be processed by the system
    srrg_core::RawDepthImage _image_depth;
    srrg_core::RGBImage      _image_rgb;
    Eigen::Matrix3f          _camera_matrix;
    double _timestamp;

    //! @brief ground truth from the dataset - if present
    Eigen::Isometry3f _gt_pose;
    Eigen::Isometry3f _camera_guess;
    Eigen::Isometry3f _prev_odom_reading;

    //! @brief module configuration
    Config _config;

    //! @brief jic
    bool _is_initialized;

    size_t _message_number;
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} //ia end namespace srrg_bagasha
