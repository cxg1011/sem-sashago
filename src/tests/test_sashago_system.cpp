#include <iostream>
#include <yaml-cpp/yaml.h>

#include <srrg_messages/message_reader.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_messages/sensor_message_sorter.h>
#include <srrg_messages/message_timestamp_synchronizer.h>

#include <g2o/core/block_solver.h>
#include <g2o/core/factory.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/solvers/cholmod/linear_solver_cholmod.h>
#include <g2o/solvers/csparse/linear_solver_csparse.h>
#include <g2o/core/batch_stats.h>
#include <g2o/core/robust_kernel.h>
#include <g2o/core/robust_kernel_factory.h>
#include <system/sashago_system.h>
#include <viewer/map_viewer.h>


using namespace std;
using namespace cv;
using namespace srrg_core;
using namespace srrg_sashago;

//ia test on icl
int main(int argc, char** argv) {

  QApplication qapp(argc, argv);
  MapViewer viewer;

  if (argc < 4)
    throw std::runtime_error("<programma> <depth_topic> <rgb_topic> <usegui=0/1> <filename>");

  bool show_images = false;
  int use_gui = 0;
  std::vector<string> depth_plus_rgb_topic;
  depth_plus_rgb_topic.push_back(std::string(argv[1]));
  depth_plus_rgb_topic.push_back(std::string(argv[2]));
  use_gui = atoi(argv[3]);
  std::string filename = argv[4];

  MessageReader reader;
  reader.open(filename);

  MessageTimestampSynchronizer synchronizer;
  synchronizer.setTopics(depth_plus_rgb_topic);
  synchronizer.setTimeInterval(0.03);

  RGBImage rgb_image;
  RawDepthImage depth_image;

  // bdc instantiate the bagasha system
  SashagoSystem system;
  system.mutableConfig().verbosity = VerbosityLevel::Debug;

  // bdc get tracker | map | relocalizer for configuration
  ShapeTracker* tracker = system.tracker();
  WorldMap* map = system.map();
  SashagoSystem::SceneContainer* scenes = system.scenes();
  ShapeDetector* detector = system.detector();
  Relocalizer* relocalizer = system.relocalizer();

  //ia setup tracker
  tracker->aligner()->mutableConfig().use_support_weight = true;
  tracker->aligner()->solver().setPointKernelThreshold(0.01);
  tracker->aligner()->solver().setDirectionKernelThreshold(0.01);

  // bdc setup map
  map->mutableConfig().age_threshold_point = 8;
  map->mutableConfig().age_threshold_line = 8;
  map->mutableConfig().age_threshold_plane = 20;

  // bdc once set the configurations, init the system
  system.init();
  
  //ia set up viewer
  viewer.setMachableMap(map);
  viewer.setSceneContainer(scenes);

  if (use_gui)
    viewer.show();
  
  //ia start reading stuff
  while (reader.good() && !viewer.terminationRequested()) {
    BaseMessage* msg = reader.readMessage();
    if (!msg)
      continue;
    
    BaseImageMessage* base_img = dynamic_cast<BaseImageMessage*>(msg);
    if (!base_img) {
      delete msg;
      continue;
    }

    synchronizer.putMessage(base_img);

    if (!synchronizer.messagesReady())
      continue;

    PinholeImageMessage* depth_msg = 0;
    PinholeImageMessage* rgb_msg = 0;

    depth_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[0].get());
    if (!depth_msg)
      throw std::runtime_error("depth msg expected. Shall thou burn in hell.");

    rgb_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[1].get());

    if (!depth_msg || !rgb_msg)
      continue;

    depth_msg->image().copyTo(depth_image);
    rgb_msg->image().copyTo(rgb_image);

    if(show_images) {
      cv::imshow("rgb_image",rgb_image);
      cv::imshow("depth_image",depth_image);
      cv::waitKey(10);
    }
   
    //ia initialize detector
    if (!detector->isCameraMatrixSet()) {
      std::cerr << FG_RED("setup camera matrix") << std::endl;
      const Eigen::Matrix3f K = depth_msg->cameraMatrix();

      std::cerr << "camera matrix just read\n" << depth_msg->cameraMatrix() << std::endl;
      std::cerr << "camera K read\n" << K << std::endl;

      detector->setK(K);
      detector->setRowsAndCols(rgb_image.rows,rgb_image.cols);
      detector->init();
    }
   
    //ia initialize the tracker with the fresh images
    system.setImages(rgb_image, depth_image, rgb_msg->timestamp());
    system.compute();

    //ia viewer update
    if (use_gui) {
      if (viewer.isVisible()) {
        viewer.updateGL();
        qapp.processEvents();

        while (viewer.pausePlayback()) {
          //ia busy waiting
          viewer.updateGL();
          qapp.processEvents();
        }
      }
    }

    std::cerr << FG_YELLOW(DOUBLE_BAR) << std::endl << std::endl;

  }


  // bdc save generated graph
  system.saveGraph("porco_dio_graph.jesus");
  
  return 0;
}


