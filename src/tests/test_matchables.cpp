#include <iostream>
#include "types/scene.h"
#include "types/world_map.h"

#define NUM_POINTS 10
#define NUM_LINES  10
#define NUM_PLANES 10

using namespace srrg_sashago;

int main(int argc, char** argv) {
  Scene scene;

  for (size_t i = 0; i < NUM_POINTS; ++i) {
    Matchable m(MatchableBase::Type::Point, Vector3::Random());
    scene.addEntry(m);
  }

  for (size_t i = 0; i < NUM_LINES; ++i) {
    Matchable m(MatchableBase::Type::Line, Vector3::Random(), Matrix3::Identity());
    scene.addEntry(m);
  }

  for (size_t i = 0; i < NUM_PLANES; ++i) {
    Matchable m(MatchableBase::Type::Plane, Vector3::Random(), Matrix3::Identity());
    scene.addEntry(m);
  }

  WorldMap map;
  map.mutableConfig().age_threshold_point = 0;
  map.mutableConfig().age_threshold_line = 0;
  map.mutableConfig().age_threshold_plane = 0;

  for (SceneEntry* e : scene.entries()) {
    Landmark* l = map.addEntry(e);
    if (l) {
      std::cerr << "ce ne ho uno nuovo ed e' questo:" << std::endl;
      std::cerr << l->entry()->matchable() << std::endl;
    } else {
      std::cerr << "non ce ne ho puttane" << std::endl;
    }
  }

}
