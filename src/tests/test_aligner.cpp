#include <iostream>
#include "aligner/shapes_aligner.h"

using namespace srrg_sashago;

#define NUM_POINTS 10
#define NUM_LINES  10
#define NUM_PLANES 10

int main(int argc, char** argv) {
  const Vector3 p_t(0.05,0.05,0.05);
  const Matrix3 r_t = Matrix3::Identity();
  Matrix3 r = Matrix3::Identity();

  Scene scene_fixed;
  Scene scene_moving;

  for (size_t i = 0; i < NUM_POINTS; ++i) {
    Vector3 point = Vector3::Random();
    Matchable m_fixed(MatchableBase::Type::Point, point);
    Matchable m_moving(MatchableBase::Type::Point, point+p_t);
    scene_fixed.addEntry(m_fixed);
    scene_moving.addEntry(m_moving);
  }

  for (size_t i = 0; i < NUM_LINES; ++i) {
    Isometry3 iso_t = Isometry3::Identity();
    iso_t.linear() = r_t;
    iso_t.translation() = p_t;

    Vector3 point = Vector3::Random();
    Matchable m_fixed(MatchableBase::Type::Line, point, r);
    Matchable m_moving = m_fixed.transform(iso_t);

    scene_fixed.addEntry(m_fixed);
    scene_moving.addEntry(m_moving);
  }

  for (size_t i = 0; i < NUM_PLANES; ++i) {
    Isometry3 iso_t = Isometry3::Identity();
    iso_t.linear() = r_t;
    iso_t.translation() = p_t;

    Vector3 point = Vector3::Random();
    Matchable m_fixed(MatchableBase::Type::Plane, point, r);
    Matchable m_moving = m_fixed.transform(iso_t);

    scene_fixed.addEntry(m_fixed);
    scene_moving.addEntry(m_moving);
  }

  WorldMap map;
  map.mutableConfig().age_threshold_point = 0;
  map.mutableConfig().age_threshold_line = 0;
  map.mutableConfig().age_threshold_plane = 0;

  for (SceneEntry* e : scene_fixed.entries()) {
    Landmark* l = map.addEntry(e);
    if (l) {
      std::cerr << "ce ne ho uno nuovo ed e' questo:" << std::endl;
      std::cerr << l->entry()->matchable() << std::endl;
    } else {
      std::cerr << "non ce ne ho puttane" << std::endl;
    }

  }

  ShapesAligner aligner;
  aligner.mutableConfig().verbosity = VerbosityLevel::Debug;

  map.updatePool();

  aligner.setFixedMap(&map);
  aligner.setMovingScene(&scene_moving);
  aligner.setT(scene_fixed.pose());
  aligner.compute();

  std::cerr << "FINAL T:\n" << aligner.T().matrix() << std::endl;

  scene_moving.setPose(aligner.T());

  map.clearPool();


  return 0;
}




