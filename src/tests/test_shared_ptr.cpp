#include <iostream>
#include "srrg_system_utils/system_utils.h"
#include "types/scene_entry.h"

using MatchablePtr = std::shared_ptr<srrg_sashago::Matchable>;
using MatchablePtrVector = std::vector<srrg_sashago::Matchable*, Eigen::aligned_allocator<srrg_sashago::Matchable*> >;
using MatchableSharedPtrVector = std::vector<MatchablePtr>;

int main(int argc, char** argv) {
  if (argc < 2)
    throw std::runtime_error("insert how many matchables");

  const uint64_t num_items = atoi(argv[1]);
  const Eigen::Vector3f point = Eigen::Vector3f::Random();
  const srrg_sashago::Matchable::Type type = srrg_sashago::Matchable::Type::Point;

  double t0 = 0;

  std::cerr << "profiling tests" << std::endl << std::endl;


  //ia pre-allocated vector
  t0 = srrg_core::getTime();
  MatchableSharedPtrVector* shared_vec = new MatchableSharedPtrVector(num_items);
  std::cerr << "[shared] vector time = " << srrg_core::getTime() - t0 << std::endl;
  
  t0 = srrg_core::getTime();
  MatchablePtrVector* normal_vec = new MatchablePtrVector(num_items);
  std::cerr << "[normal] vector time = " << srrg_core::getTime() - t0 << std::endl;

  t0 = srrg_core::getTime();
  for (size_t i = 0; i < num_items; ++i) {
    MatchablePtr sp(new srrg_sashago::Matchable(type, point));
    (*shared_vec)[i] = sp;
  }
  std::cerr << "[shared] allocate = " << srrg_core::getTime() - t0 << std::endl;

  t0 = srrg_core::getTime();
  for (size_t i = 0; i < num_items; ++i) {
    srrg_sashago::Matchable* p = new srrg_sashago::Matchable(type, point);
    (*normal_vec)[i] = p;
  }
  std::cerr << "[normal] allocate = " << srrg_core::getTime() - t0 << std::endl;


  //ia delete the objects
  t0 = srrg_core::getTime();
  delete shared_vec;
  std::cerr << "[shared] delete = " << srrg_core::getTime() - t0 << std::endl;

  t0 = srrg_core::getTime();
  for (size_t i = 0; i < num_items; ++i) {
    delete (*normal_vec)[i];
  }
  delete normal_vec;
  std::cerr << "[normal] delete = " << srrg_core::getTime() - t0 << std::endl;


  //ia push back on the vector - what is done in the detectors
  MatchableSharedPtrVector stack_shared_vec;
  MatchablePtrVector stack_normal_vec;

  t0 = srrg_core::getTime();
  for (size_t i = 0; i < num_items; ++i) {
    stack_shared_vec.push_back(MatchablePtr(new srrg_sashago::Matchable(type, point)));
  }
  std::cerr << "[shared] push back = " << srrg_core::getTime() - t0 << std::endl;

  
  t0 = srrg_core::getTime();
  for (size_t i = 0; i < num_items; ++i) {
    stack_normal_vec.push_back(new srrg_sashago::Matchable(type, point));
  }
  std::cerr << "[normal] push back = " << srrg_core::getTime() - t0 << std::endl;  


  //ia access
  t0 = srrg_core::getTime();
  for (size_t i = 0; i < num_items; ++i) {
    const Eigen::Vector3f& p = stack_shared_vec[i].get()->point;
  }
  std::cerr << "[shared] access = " << srrg_core::getTime() - t0 << std::endl;

  
  t0 = srrg_core::getTime();
  for (size_t i = 0; i < num_items; ++i) {
    const Eigen::Vector3f& p = stack_normal_vec[i]->point;
  }
  std::cerr << "[normal] access = " << srrg_core::getTime() - t0 << std::endl;  


  //ia copy
  t0 = srrg_core::getTime();
  MatchableSharedPtrVector shared_copy(stack_shared_vec);
  std::cerr << "[shared] copy = " << srrg_core::getTime() - t0 << std::endl;    

  t0 = srrg_core::getTime();
  MatchablePtrVector normal_copy(stack_normal_vec);
  std::cerr << "[normal] copy = " << srrg_core::getTime() - t0 << std::endl;    
  
  for (size_t i = 0; i < num_items; ++i) {
    delete stack_normal_vec[i];
  }
}

