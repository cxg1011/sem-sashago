#include "line_detector.h"

namespace srrg_sashago {

  using namespace std;
  using namespace srrg_core;
  using namespace cv;
  using namespace cv::line_descriptor;
  
  LineDetector::LineDetector(){
    _initialized = false;
    _good_lines = false;
  }

  LineDetector::~LineDetector() {}

  void LineDetector::init(){
    _ls = LSDDetector::createLSDDetector();
    _bd = BinaryDescriptor::createBinaryDescriptor();
    _bd->setWidthOfBand(_configuration.width_of_band);
    
    _initialized = true;
    _good_lines = false;
  }

  bool LineDetector::lp2t(const int num_of_pixels){
    return num_of_pixels > _configuration.lp2t_threshold;
  }

  bool LineDetector::bp2d(const cv::Vec3f &pA,
                          const cv::Vec3f &pB,
                          const cv::Vec3f &pC){
    return ((cv::norm((pC-pA).cross(pC-pB))/cv::norm(pB-pA))<_configuration.bp2d_threshold);
  }

  void LineDetector::computeLineRotationMatrix(Eigen::Matrix3f& rotation_matrix,
                                               const Eigen::Vector3f& direction){

    float d = sqrt(direction.x()*direction.x() + direction.y()*direction.y());

    const float& dirx = direction.x();
    const float& diry = direction.y();
    const float& dirz = direction.z();
    
    if(d > std::numeric_limits<float>::min()) {
        rotation_matrix << diry/d,     dirx*dirz/d,     dirx,
            -dirx/d,   diry*dirz/d, diry,
            0,          -d,        dirz;
      } else {
        rotation_matrix.setIdentity();
      }

  }

  void LineDetector::compute(Scene* scene_,
                             const UnsignedCharImage& gray_image_,
                             const Float3Image& points_image_){

    std::vector<KeyLine> lines;
    _ls->detect(gray_image_, lines, _configuration.scale, _configuration.num_octaves);

    cv::Mat descriptors;
    _bd->compute(gray_image_, lines, descriptors);

    const int lines_size = lines.size();

    for(int i=0; i < lines_size; ++i){

        const KeyLine& line = lines[i];
        
        if(!lp2t(line.numOfPixels))
          continue;

        cv::Point ptA = cv::Point2f(line.startPointX, line.startPointY);
        cv::Point ptB = cv::Point2f(line.endPointX, line.endPointY);

        const cv::Vec3f& pA = points_image_.at<cv::Vec3f>(ptA.y, ptA.x);
        const cv::Vec3f& pB = points_image_.at<cv::Vec3f>(ptB.y, ptB.x);
        if(cv::norm(pA) < 1e-2 ||
           cv::norm(pB) < 1e-2)
          continue;

        // get the mid-point of the line
        const cv::Vec3f pC = points_image_.at<cv::Vec3f>(line.pt.y,
                                                          line.pt.x);

        if(!bp2d(pA,pB,pC))
          continue;

        const Eigen::Vector3f start_point(pA(0), pA(1), pA(2));
        const Eigen::Vector3f end_point(pB(0), pB(1), pB(2));

        Eigen::Vector3f direction = end_point-start_point;
        float length = direction.norm();
        direction /= length;

        srrg_core::Cloud3D cloud;
        cv::LineIterator line_iterator(points_image_, ptA, ptB);//, 8);
        const int line_pxl_size = line_iterator.count;
        cloud.resize(line_pxl_size);
        
        for(size_t pxl = 0; pxl < line_pxl_size; ++pxl, ++line_iterator ) {
          const cv::Vec3f& p3d = points_image_.at<cv::Vec3f>(line_iterator.pos());
          cloud[pxl] = srrg_core::RichPoint3D(Eigen::Vector3f(p3d[0], p3d[1], p3d[2]), Eigen::Vector3f(0,0,0), 1.f);
          // std::cerr << "l pt: " << cloud[pxl].point().transpose() << std::endl;
        }


        //ia clean, no new
        Matchable line_matchable(MatchableBase::Type::Line, start_point);
        line_matchable.setRotationFromDirection(direction);

        SceneEntry* line_entry = scene_->addEntry(line_matchable);
        line_entry->setExtent(Eigen::Vector2f(length,0));
        line_entry->setDescriptor(descriptors.row(i));
        line_entry->setCloud(cloud);

		// This is for semantics, take the midpoint and hope for the best, I guess?
		line_entry->setSemaRep(line.pt);	// To figure out the BB stuff later
      }
  }

} //ia end namespace srrg_bagasha
