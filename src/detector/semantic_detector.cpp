#include "semantic_detector.h"

using namespace std;
using namespace cv;

namespace srrg_sashago
{

SemanticDetector::SemanticDetector()
{
}

SemanticDetector::~SemanticDetector()
{
}

SemaDetectionSet SemanticDetector::runBBDetector(double timestamp, string gtPath)
{
#ifdef USE_SEMANTIC_GT
	assert(gtPath.length() > 0 && "Groundtruth path needs to be non-empty!");
	return loadBBFromPath(timestamp, gtPath);
#else
	cerr << "Semantic requests not implemented yet!" << endl;
	throw bad_function_call();
#endif
}

void SemanticDetector::initBBRegistry()
{
	cout << "[SemanticDetector]::Initializing!!" << endl;
	// Clear detection registry
	_detections.clear();
	
	// Now look for detections in the semantic detections file matching the
	// above timestamp
	ifstream ifs(_config.gt_file_path);
	string line;
	while (getline(ifs, line)) {
		// Line format is assumed to be:
		// <timestamp>\t[<bb_x1>, <bb_y1>, <bb_x2>, <bb_y2>]\t<class_ID>\t<score>\n
		
		// Get timestamp
		istringstream iss(line);
		double lts; iss >> lts;
		char buff[4];
		iss.get(buff, 3);	// WARNING: this thing extracts n-1 characters and appends '\0'!
		
		// Get bounding box coordinates
		vector<float> bb_coords(4);
		for (size_t i=0; i<4; i++) {
			iss >> bb_coords[i];
			iss.get(buff, 3);
		}
		Rect bb = Rect(
				Point(bb_coords[0]-_config.box_padding, bb_coords[1]-_config.box_padding),
				Point(bb_coords[2]+_config.box_padding, bb_coords[3]+_config.box_padding));
		// Get detection score and class
		int classID; iss >> classID;
		iss.get();
		float score; iss >> score;

		// Check with configuration threshold to see if the detection is valid
		if (score < _config.conf_thresh) continue;

		// Check if this timestamp already has detections in the map
		SemaGTMap::iterator seeker = _detections.find(lts);
		if (seeker == _detections.end()) {	// If it doesn't, create a new entry
			SemaDetectionSet ret;
			ret.timestamp = lts;
			ret.bbs.emplace_back(bb);
			ret.scores.push_back(score);
			ret.classIDs.push_back(classID);
			_detections.insert(SemaGTMap::value_type(lts, ret));
		} else {	// If it does, append to the existing entry
			seeker->second.pushBackData(bb, score, classID);
		}
	}
}

SemaDetectionSet SemanticDetector::getSemaInfoForTime(double timestamp)
{
	if (_detections.find(timestamp) == _detections.end()) {
		// timestamp has no usable detections
		SemaDetectionSet ret;
		ret.timestamp = -1;
		return ret;
	} else {
		// timestamp has usable detections
		return _detections[timestamp];
	}

	throw exception();
}


SemaDetectionSet SemanticDetector::loadBBFromPath(double timestamp, string gtPath)
{
	// Semantic detection set to return.
	SemaDetectionSet ret;

	/*** Used in case timestamp is part of the filename ***/
	// Assuming that the image is in a path of the form: .../<timestamp>.<filetype>
	//string file = imgPathName.substr(imgPathName.rfind('/')+1);
    //string tar_ts = file.substr(0, file.rfind('.'));	// Get frame timestamp as a string
	// ... then cast to double or something

	ret.timestamp = timestamp;

	// Now look for detections in the semantic detections file matching the
	// above timestamp
	ifstream ifs(gtPath);
	string line;
	bool found_timestamp = false;
	while (getline(ifs, line)) {
		// Line format is assumed to be:
		// <timestamp>\t[<bb_x1>, <bb_y1>, <bb_x2>, <bb_y2>]\t<class_ID>\t<score>\n
		
		// Get timestamp
		istringstream iss(line);
		double lts; iss >> lts;
		char buff[4];
		iss.get(buff, 3);	// WARNING: this thing extracts n-1 characters and appends '\0'!
		if (lts != timestamp && ! found_timestamp)
			continue;
		else if (lts != timestamp && found_timestamp)
			break;
		else 	// lts == timestamp
			found_timestamp = true;

		// Get bounding box coordinates
		vector<float> bb_coords(4);
		for (size_t i=0; i<4; i++) {
			iss >> bb_coords[i];
			iss.get(buff, 3);
		}
		
		
		// Get detection score and class
		int classID; iss >> classID;
		iss.get();
		float score; iss >> score;

		// Check with configuration threshold to see if the detection is valid
		if (score < _config.conf_thresh) continue;
		
		ret.bbs.emplace_back(
				Point(bb_coords[0]-_config.box_padding, bb_coords[1]-_config.box_padding),
				Point(bb_coords[2]+_config.box_padding, bb_coords[3]+_config.box_padding));
		ret.scores.push_back(score);
		ret.classIDs.push_back(classID);
	}

	return ret;
}

} // namespace srrg_sashago
