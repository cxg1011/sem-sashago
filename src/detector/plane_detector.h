#pragma once
#include <Eigen/Geometry>
#include <Eigen/Eigenvalues>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <srrg_types/cloud_3d.h>
#include <srrg_image_utils/depth_utils.h>
#include <srrg_image_utils/point_image_utils.h>
#include <srrg_system_utils/system_utils.h>
#include <srrg_path_map/clusterer_path_search.h>

#include "types/scene.h"

namespace srrg_sashago {

  class PlaneDetector{
  public:
    enum PixelType {Surfel=0,Line=-1,Plane=2};

    struct Config {
      int min_cluster_points;

      Config() {
        min_cluster_points = 1000;
      }
    };

    //! @brief ctor/dtor
    PlaneDetector();
    virtual ~PlaneDetector();

    //! @brief configuration
    inline const Config& config() const {return _configuration;}
    inline Config& mutableConfig() {return _configuration;}

    //! @brief initializes the things
    void init();

    //! @brief adds plane matchable to the scene
    void compute(Scene* scene_,
                 srrg_core::IntImage& regions_image,
                 const srrg_core::Float3Image& points_image,
                 const srrg_core::Float3Image& normals_image,
                 const srrg_core::FloatImage& curvature_image);

    inline const srrg_core::RGBImage& planesImage() const {return _planes_image;}

    void computePlaneRotationMatrix(Eigen::Matrix3f& rotation_matrix,
                                    const Eigen::Vector3f& direction);

  protected:
    void extractClusters(srrg_core::ClustererPathSearch::ClusterVector& clusters,
                         const srrg_core::IntImage& regions_image);

    void generatePlanefromCluster(Scene* scene_,
                                  srrg_core::IntImage& regions_image,
                                  const srrg_core::Float3Image& points_image,
                                  const srrg_core::Float3Image& normals_image,
                                  const srrg_core::ClustererPathSearch::Cluster& cluster_);

    Config _configuration;
    bool _initialized;
    srrg_core::RGBImage _planes_image;
    
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  };
} //ia end namespace srrg_bagasha
