#include "point_detector.h"
#include <iostream>

namespace srrg_sashago  {

  PointDetector::PointDetector() {
    _initialized = false;
    _number_of_detectors = 1;
    _initialized = false;
    _image_rows = 0;
    _image_cols = 0;

    _number_of_cols_bin = 1;
    _number_of_rows_bin = 1;
    _target_number_of_keypoints = 0;
    _target_number_of_keypoints_per_detector = 0;

    _image_rows = 1;
    _image_cols = 1;
  }

  PointDetector::~PointDetector() {
    if(_detectors && _detector_regions && _detector_thresholds) {
      for (size_t r = 0; r < _configuration.vertical_detectors; ++r) {
        delete[] _detectors[r];
        delete[] _detector_regions[r];
        delete[] _detector_thresholds[r];
      }
      delete[] _detectors;
      delete[] _detector_regions;
      delete[] _detector_thresholds;
    }
  }


  // From ProSLAM w/ love
  void PointDetector::_initDetectors() {

    if(!_image_rows || !_image_cols)
      std::runtime_error("[PointDetector::_initDetectors]| first set image rows and cols");
    
    //bdc initializing detectors (per regions)
    _detectors           = new cv::Ptr<cv::FastFeatureDetector>*[_configuration.vertical_detectors];
    _detector_regions    = new cv::Rect*[_configuration.vertical_detectors];
    _detector_thresholds = new real*[_configuration.vertical_detectors];
    // TODO we need the image size to specify detectors rows/cols
    const real rows_per_detector   = (real)_image_rows/_configuration.vertical_detectors;
    const real colums_per_detector = (real)_image_cols/_configuration.horizontal_detectors;
    for(size_t r = 0; r < _configuration.vertical_detectors; ++r) {
      _detectors[r]           = new cv::Ptr<cv::FastFeatureDetector>[_configuration.horizontal_detectors];
      _detector_regions[r]    = new cv::Rect[_configuration.horizontal_detectors];
      _detector_thresholds[r] = new real[_configuration.horizontal_detectors];
      for(size_t c = 0; c < _configuration.horizontal_detectors; ++c) {
        _detectors[r][c] = cv::FastFeatureDetector::create(_configuration.detector_threshold_minimum,
                                                           _configuration.nonmax_suppression);
        _detector_regions[r][c] = cv::Rect(std::round(c*colums_per_detector),
                                    std::round(r*rows_per_detector),
                                    colums_per_detector,
                                    rows_per_detector);
        _detector_thresholds[r][c] = _configuration.detector_threshold_minimum;
      }
    }
    _number_of_detectors = _configuration.vertical_detectors * _configuration.horizontal_detectors;
  
    //ds compute binning configuration
    _number_of_cols_bin = std::floor((real)_image_cols/_configuration.bin_size_pixels)+1;
    _number_of_rows_bin = std::floor((real)_image_rows/_configuration.bin_size_pixels)+1;

    //ds compute target number of points
    _target_number_of_keypoints = _number_of_cols_bin*_number_of_rows_bin;

    //ds compute target points per detector region
    _target_number_of_keypoints_per_detector = (real)_target_number_of_keypoints/_number_of_detectors;
   
  }
  
  void PointDetector::init() {

    //bdc initialize the detectors
    _initDetectors();

    if(_configuration.descriptors_type == "BRIEF") {
#ifdef SRRG_OPENCV_CONTRIB_FOUND
      _descriptor_extractor = cv::xfeatures2d::BriefDescriptorExtractor::create();
#else
      std::cerr << "[PointDetector][init]: WARNING, using ORB instead of BRIEF since xfeatures2d is not found";
      _descriptor_extractor = cv::ORB::create();
#endif
    } else if(_configuration.descriptors_type == "ORB") {
      _descriptor_extractor = cv::ORB::create();
    } else {
      throw std::runtime_error("[PointDetector][init]: supported descriptors are: {ORB, BRIEF}");
    }
    
    
    _initialized = true;
    
  }

  void PointDetector::_adjustDetectorThresholds() {
    for (size_t r = 0; r < _configuration.vertical_detectors; ++r) {
      for (size_t c = 0; c < _configuration.horizontal_detectors; ++c) {
        _detectors[r][c]->setThreshold(_detector_thresholds[r][c]);
        //std::cerr << _detectors[r][c]->getThreshold() << " ";
      }
    }
    std::cerr << std::endl;
  }

  
  void PointDetector::_detectKeypoints(const srrg_core::RGBImage& rgb_image_,
                                       std::vector<cv::KeyPoint>& keypoints_) {
    //bdc for all the regions
    for(size_t r = 0; r < _configuration.vertical_detectors; ++r) {
      for(size_t c = 0; c < _configuration.horizontal_detectors; ++c) {
        std::vector<cv::KeyPoint> keypoints_current_detector(0);
        _detectors[r][c]->detect(rgb_image_(_detector_regions[r][c]),
                                 keypoints_current_detector);
        
        real detector_threshold = _detectors[r][c]->getThreshold();

        //ds compute point delta: 100% loss > -1, 100% gain > +1
        const int diff = keypoints_current_detector.size() - _target_number_of_keypoints_per_detector;
        const real delta = (real)diff/(real)_target_number_of_keypoints_per_detector;
        
        //ds check if there's a significant loss of target points (delta is negative)
        if (delta < -_configuration.target_number_of_keypoints_tolerance) {

          //ds compute new, lower threshold, capped and damped
          const real change = std::max(delta, -_configuration.detector_threshold_maximum_change);

          //ds always lower threshold by at least 1
          detector_threshold += std::min(change*detector_threshold, -1.f);

          //ds check minimum threshold
          if (detector_threshold < _configuration.detector_threshold_minimum) {
            detector_threshold = _configuration.detector_threshold_minimum;
          }
        }

        //ds or if there's a significant gain of target points (delta is positive)
        else if (delta > _configuration.target_number_of_keypoints_tolerance) {

          //ds compute new, higher threshold - capped and damped
          const real change = std::min(delta, _configuration.detector_threshold_maximum_change);

          //ds always increase threshold by at least 1
          detector_threshold += std::max(change*detector_threshold, 1.f);

          //ds check maximum threshold
          if (detector_threshold > _configuration.detector_threshold_maximum) {
            detector_threshold = _configuration.detector_threshold_maximum;
          }
        }

        //ds set treshold (no effect if not changed)
        _detector_thresholds[r][c] = detector_threshold;

        //ds shift keypoint coordinates to whole image region
        const cv::Point2f& offset = _detector_regions[r][c].tl();
        std::for_each(keypoints_current_detector.begin(),
                      keypoints_current_detector.end(),
                      [&offset](cv::KeyPoint& keypoint_) {
                        keypoint_.pt += offset;
                      });

        //ds add to complete vector
        keypoints_.insert(keypoints_.end(),
                          keypoints_current_detector.begin(),
                          keypoints_current_detector.end());
      }      
    }
    std::cerr << std::endl;
  }

  
  void PointDetector::compute(Scene* scene_,
                              const srrg_core::RGBImage& rgb_image_,
                              const srrg_core::Float3Image& points_image_,
                              const srrg_core::Float3Image& normals_image_) {
    if(!_initialized)
      throw std::runtime_error("[PointDetector][compute]: not initialized");
    
    // keypoint buffer
    std::vector<cv::KeyPoint> keypoints;
    cv::Mat descriptors;
    // detect keypoints
    _detectKeypoints(rgb_image_, keypoints);
    _adjustDetectorThresholds();
    _descriptor_extractor->compute(rgb_image_, keypoints, descriptors);
    
    const int keypoints_size = keypoints.size();
    if(!keypoints_size){
      // std::cerr << "warning: [PointDetector] found 0 keypoints!\n";
      return;
    }
    
    // Get 3d points and generate matchables
    for(size_t i = 0; i < keypoints_size; ++i) {
      const cv::Point2f& keypoint = keypoints[i].pt;
      const cv::Vec3f point = points_image_.at<cv::Vec3f>(keypoint);
      const cv::Vec3f normal = normals_image_.at<cv::Vec3f>(keypoint);
      const float depth = cv::norm(point);

      if(depth < 1e-2 || cv::norm(normal) < 1e-2) {
        continue;
      }
      const Eigen::Vector3f p(point(0), point(1), point(2));
      const Eigen::Vector3f n(normal(0), normal(1), normal(2));

      //ia clean, interior peace
      Matchable point_matchable(MatchableBase::Type::Point, p);
      SceneEntry* point_entry = scene_->addEntry(point_matchable);
      point_entry->setDescriptor(descriptors.row(i));
	  point_entry->setSemaRep(keypoint);
    }
  }
} //ia end namespace srrg_bagasha
