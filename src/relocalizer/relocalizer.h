#pragma once
#include <srrg_hbst/types/binary_tree.hpp>

#include "types/scene.h"
#include "types/constraint.h"

#define DESCRIPTOR_SIZE_BITS 256

namespace srrg_sashago {

  // bdc, guess what? we already have the Matchables in our code...
  typedef srrg_hbst::BinaryMatchable<SceneEntry*, DESCRIPTOR_SIZE_BITS> BinaryEntry;
  typedef srrg_hbst::BinaryNode<BinaryEntry> BinaryEntryNode;
  typedef srrg_hbst::BinaryNode<BinaryEntry>::MatchableVector BinaryEntryVector;
  typedef srrg_hbst::BinaryTree<BinaryEntryNode> Tree;

  class Relocalizer {
  public:
    struct Config {
      VerbosityLevel verbosity;
      size_t maximum_matching_distance;     //! @brief maximum hamming distance to get an association
      size_t frame_interspace;              //! @brief minimum amount of frames that must pass to detect closures
      size_t min_relocalization_matches;    //! @brief minimum amount of matches to mark this scene as a match
      real min_relocalization_score;        //! @brief minimum ratio between matches and number of entries in the scene
      
      Config() {
        verbosity = VerbosityLevel::Info;
        maximum_matching_distance = 25;
        frame_interspace = 100;
        min_relocalization_score = 0.33;
        min_relocalization_matches = 5;
      }
    };

    Relocalizer();
    virtual ~Relocalizer();

    //! @brief inline stuff
    inline const Config& config() const {return _config;}
    inline Config& mutableConfig() {return _config;}

    inline const ConstraintVector& constraints() const {return _constraints;}
    inline Scene* matchingScene() const {return _matching_scene;}

    //! @brief set current scene
    inline void setScene(Scene* scene_) { _scene = scene_;}
    inline void setSceneContainer(IntSceneMap* scenes_) { _scenes = scenes_;}

    //! matchAndAdd current scene to the tree
    void compute();
    
  protected:
    //! @brief hbst tree used for closure detection
    Tree _hbst_tree;

    //! @brief pointer to current scene
    Scene* _scene = 0;
    Scene* _matching_scene = 0;

    //! @brief pointer to the set of scenes
    IntSceneMap* _scenes = 0;

    //! @brief constraints deriving from relocalization
    ConstraintVector _constraints;

    //! @brief ugly config
    Config _config;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} //ia end namespace

